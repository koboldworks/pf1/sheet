# Changelog

## 0.14.0

- Fix: Buff type was empty for certain buffs.
- New: Buff end timing is now displayed.
- i18n: Invalid string for spell focus component.
- Fix: Spell compoonent display was empty
- New: Weapons, Equipment (Shield, Armor, etc.), and Loot display their material (not addons tho).

## 0.13.1

- Fix: Various display issues with container contents, changes, and others.
- Fix: Range displaying "up to undefined"
- Fix: Effect/Footnotes were not enriched in infosheet.

## 0.13.0

- Change: PF1 v10 compatibility.
  - Only info sheet received proper work.
  - Statsheet is likely poorly functional but should not prevent opening the sheets.
- Fix: Typo in i18n string for round time key.

## 0.12.0

### InfoSheet

- New: Added class association display. [#38]
- Fix: Range display for metric. [#40]
- Fix: Range display included odd numbers or undefined. [#36]
- New: Ammunition display [#33]
- Change: Learned at is hidden on actors and level is displayed instead. [#35]
- Fix: Natural attacks no longer display broken and masterwork states. [#39]
- Fix: Weight units. [#32]
- Fix: Excess dictionary/boolean flag display. [#34]
- New: Display ability score bonus to damage. [#31]
- New: Display touch attack status. [#30]
- New: Display script calls. [#27]
- New: Links in descriptions now display target URL in tooltip. [#25]
- New: All actions are displayed instead of default only. [#29]

### DataSheet

- Fix: Adjusting data did not update sheet correctly. [#10]
- Fix: Adjusting numeric values caused odd behaviour. [#11]
- Fix: Removed number of unnecessary re-renders.

## 0.11.2

### InfoSheet

- Fix: Error when encountering missing save DC offset formula on spells.
- Fix: Spell sheet was unopenable in PF1v9
- Fix: Activation cost display if it required 1 or less actions was corrupt.
- Fix: Spell descriptions were not shown.
- Fix: Action ranges were shown incorrectly.

## 0.11.1

### InfoSheet

- Change: Checkboxes are now custom for display only boxes instead of barely visible disabled checkboxes.

### StatSheet

- Fix: PF1v9 compatibility.
  - Inability to open actors with new format DR/ER.

## 0.11.0

- Change: InfoSheet soft override is now client-side setting.
- Fix: Tags were displayed poorly.
- PF1 v9 compatibility. Support for older versions dropped.

### StatSheet

- Fix: Multiple senses rendering weird.

### InfoSheet

- Fix: Errors from missing damage instance data.
- Fix: Errors from missing attack and damage ability score.
- New: Display missing targets in Changes clearly.
- New: Allow painting various pieces of text.

## 0.10.4

- PF1 0.82.3 compatibility.
- Minimum required version upgraded to 0.82.4

## 0.10.3

### Statblock Sheet

- Fix: Missing attack items.
- Fix: Spell usage count was almost always infinite.
- Minor layout improvements.
- Added cursor change to actions.
- All tooltips now use Foundry tooltip manager.

## 0.10.2

### Statblock Sheet

- Fix: Weapons with actions were not shown.
- Fix: Skill editing did not work.
- Minor layout improvements.

## 0.10.1

- Fixed large number of issues with InfoSheet [#14, #15, #16, #17, #18, #19, #20]

## 0.10.0.2

### InfoSheet

- Fix: Unidentified items displayed their identified name to players.
- Fix: Identified checkbox did not reflect the actual identified state.

## 0.10.0.1

- Fix: StatBlock sheet was broken with last release.

## 0.10.0

- New: InfoSheet - compact item sheet for all item types.
  - New: InfoSheet Soft Default - forces InfoSheet to be the default sheet for all items not on actor and don't have any other specific sheet assigned to them. This can be disabled.

## 0.9.0.1

### Statblock Sheet

- Fix: Notes section was nonfunctional.
- Fix: Drag&drop into the sheet did not work.
- Fix: Automatic slots actually controlled ability score bonus to slots.
- Fix: Weird behaviour in spellbook configuration dialog.

## 0.9.0

- Fix: Dropping spell to a spellbook sheet failed.
- Foundry v10 compatibility (only statblock sheet confirmed).
- PF1 0.82.2 compatibility (only statblock sheet confirmed).

## 0.8.0.1

- Quick fix for new sense format in PF1 0.80.22. This version is incompatible with older versions of PF1 as such.

## 0.8.0

### Statblock Sheet

- New: Ability score linking configuration
- New: Spellcasting configuration
- New: Class-based spells
- New: Gender and class list

## 0.7.0

- New: Data sheet, for your data tracking needs.
- Fixed: Spellbook hide empty toggle did not work on first few clicks after refresh.

## 0.6.0

- Changed: StatSheet shows spells by class and level instead of attack and utilty.
- New: Spell DC is displayed per level.
- New: Spell DC is displayed per spell if it differs from per level.
- Fixed: Spellbook sheet flickered on opening and updates.
- Fixed: Item drops to spellbook sheet were not functioning correctly.
- Fixed: Buff level was not shown.

## 0.5.0

- New: Spellbook sheet, for your spell sharing needs.

## 0.4.0

- New: Preserve toggle state of skills and feats on sheet update.
- New: Damage display for attacks.
- New: Drop support
- New: Ability to delete items with alt + right click. This gives a confirmation dialog before it is carried through.
- New: Equipment is displayed in three tiers: activatable, misc, and loot.
  - Activatable has most things that can be activated or consumed. Disabled and stacks with quantity of 0 are hidden.
  - Misc shows most things. Items with no direct impact (disabled or have no changes or context notes) are hidden.
  - Loot shows the rest, with everything hidden by default.
- Improved: Translation support.

## 0.3.0

- New: Iterative display.
- New: Misc category for navigation.
- New: Buff toggling.
- New: Subskill name can be altered.
- Improved: Skill and feat collapsing.
- Changed: Languages moved above skills.
- Changed: Weapon and ability attacks no longer render if none are found.

## 0.2.0

- New: Drag start for items (spells, equipment, race, ...)
- New: Offensive special abilities, other special abilities, and passive feats.
- New: Base Atk, CMB, CMD in statistics for reference. Not clickable.
- New: Collapsible skills and feats sections
- New: Buffs are displayed
- Fix: Maneuverability was not shown.
- Fix: Excess spacing removed.
- Changed: Label use and style labels for easier spotting of them.

## 0.1.0

- Initial implementation (WIP)
