# Koboldworks – Sheet-sss for Pathfinder 1e

Various additional sheets for use with the PF1 game system.

## Usage

Change sheet type of existing character.

Any character type (PC&NPC):

- `Koboldworks StatSheet`

NPC characters & basic actors:

- `Koboldworks Spellbook`
- `Koboldworks Datasheet`

Items only:

- `Koboldworks Infosheet`

## Sheets

### Statsheet

Inspired by how statblocks are displayed, but this relation is only kept at arm's length to maintain usability and clarity.

### Datasheet

Data tracking sheet.

### Spellbook

Simple spells only sheet.

### InfoSheet

Compact sheet for items.

Includes option to make this the default for items not on actors, with quick access to the actual default sheet.

## API

```js
const sheetAPI = game.modules.get("koboldworks-pf1-sheet").api;
sheetAPI.configure.abilities(actor, options); // options is optional, and is same as second parameter to render()
sheetAPI.configure.spellbooks(actor, options); // options is optional, and is same as second parameter to render()
```

## Install

Manifest URL: <https://gitlab.com/koboldworks/pf1/sheet/-/releases/permalink/latest/downloads/module.json>

Latest version for Foundry v9: <https://gitlab.com/koboldworks/pf1/sheet/-/raw/0.8.0.1/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
