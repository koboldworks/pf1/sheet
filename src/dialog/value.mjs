import { i18n } from '../utility/i18n.mjs';
import { templates } from '../config.mjs';

export async function getValueDialog({ label, value, hint, type, dtype } = {}) {
	const data = arguments[0];

	const content = await renderTemplate(templates.dialog.value, data);
	return new Promise(resolve => new Dialog({
		title: i18n.get('Koboldworks.Sheet.SetNewValue'),
		content,
		buttons: {
			set: {
				label: i18n.get('Koboldworks.Generic.Action.Set'),
				callback: html => resolve(html.querySelector('input[name="value"]').value),
			},
		},
		close: _ => resolve(undefined),
		default: 'set',
	}, {
		jQuery: false,
	})
		.render(true));
}
