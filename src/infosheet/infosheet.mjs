import { CFG, templates, flatTemplates } from '@root/config.mjs';
import { getBuffTargets } from './pf1.mjs';

import { getVisibleName, getIdentifiedName, getUnidentifiedName, getWeightInfo, getValueInfo } from '@root/utility/item.mjs';

import { i18n } from '@root/utility/i18n.mjs';

import { safeEval } from '@root/utility/roll.mjs';

const operatorLabel = {
	add: 'Koboldworks.Missing.PF1.AddOperator',
	set: 'Koboldworks.Missing.PF1.SetOperator',
	script: 'Koboldworks.Missing.PF1.ScriptOperator',
};

const chIcons = {
	add: 'fa-solid fa-plus-minus',
	set: 'fa-solid fa-equals',
	script: 'fa-brands fa-js',
};

export class InfoSheet extends ItemSheet {
	static SUPPORTED_TYPES = ['buff', 'loot', 'consumable', 'class', 'race', 'weapon', 'equipment', 'attack', 'feat', 'spell', 'container'];

	constructor(document, options = {}) {
		if (options._sheetsss?.ready) {
			super(document, options);
		}
		else {
			options._sheetsss = { ready: true };
			const cls = CFG.sheetClasses.infosheet[document.type];
			return new cls(document, options);
		}
	}

	#rollData;
	get rollData() {
		this.#rollData ??= this.item.getRollData();
		return this.#rollData;
	}

	#actRollData;
	get actionRollData() {
		this.#actRollData ??= this.item.getRollData();
		return this.#actRollData;
	}

	get title() {
		return this.visibleName;
	}

	get visibleName() { return getVisibleName(this.item); }
	get identifiedName() { return getIdentifiedName(this.item); }
	get unidentifiedName() { return getUnidentifiedName(this.item); }

	get canIdentify() {
		return !['buff', 'race', 'class', 'spell', 'attack', 'feat'].includes(this.item.type);
	}

	get template() {
		return `modules/${CFG.id}/template/sub/infosheet/${this.item.type}.hbs`;
	}

	get onActor() {
		return !!this.item.actor;
	}

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			classes: [...options.classes, 'koboldworks', 'infosheet'],
			tabs: [{ navSelector: '.actions nav', contentSelector: '.actions .action-list' }],
			width: 520,
		};
	}

	async getDescriptions(context, { showIdentified = false, canIdentify } = {}) {
		const rollData = this.rollData;

		const enrichOptions = { rollData, async: true, secrets: this.item.isOwner };

		const system = this.item.system;

		const identDesc = this.item.getDescription({ header: false }),
			unidentDesc = system.description?.unidentified;
		const validDesc = (d) => d?.length > 0 && d !== '<p></p>';

		context.hasIdentDescription = validDesc(identDesc);
		context.hasUnidentDescription = validDesc(unidentDesc);
		context.hasDescription = context.hasIdentDescription || context.hasUnidentDescription;
		context.enriched ??= {};
		context.enriched.description = (showIdentified && identDesc) ? TextEditor.enrichHTML(identDesc, enrichOptions) : null;
		context.enriched.unidentified = (canIdentify && unidentDesc && (game.user.isGM || !showIdentified)) ? TextEditor.enrichHTML(unidentDesc, enrichOptions) : null;

		context.enriched.description?.then(d => context.enriched.description = d);
		context.enriched.unidentified?.then(d => context.enriched.unidentified = d);

		await Promise.all([context.enriched.description, context.enriched.unidentified]);
	}

	async getData() {
		this.#rollData = null; // reset roll data cache

		const context = super.getData(),
			item = this.item,
			system = context.system = item.system,
			rollData = this.rollData;

		context.labels = {};
		context.actor = item.actor;

		context.distanceUnit = pf1.utils.getDistanceSystem() === 'metric' ?
			pf1.config.measureUnitsShort.m : pf1.config.measureUnitsShort.ft;
		context.weightUnit = pf1.utils.getWeightSystem() === 'metric' ?
			i18n.get('PF1.Kgs') : i18n.get('PF1.Lbs');

		context.rollData = rollData;
		context.config = pf1.config;

		setProperty(rollData, 'item.level', system.level);

		context.isEditable = this.isEditable;
		context.hasActor = !!item.actor;
		const isGM = context.isGM = game.user.isGM;

		context.isIdentified = system.identified !== false;
		const showIdentified = !item.showUnidentifiedData;
		context.isNotIdentified = item.showUnidentifiedData || isGM;
		context.unidentifiedName = this.unidentifiedName;
		const canIdentify = context.canIdentify = this.canIdentify;
		context.identifyDC = 15 + (system.cl ?? 0);

		context.name = {
			identified: this.identifiedName,
			unidentified: this.unidentifiedName,
			visible: this.visibleName,
			get identical() { return this.identified === this.unidentified; },
		};

		context.hasQuantity = Number.isFinite(system.quantity);
		context.quantity = system.quantity;

		context.cl = system.cl;
		if (system.aura) {
			context.aura = {
				school: pf1.config.spellSchools[system.aura.school] || system.aura.school,
				strength: rollData.item.auraStrength,
				strengthLabel: pf1.config.auraStrengths[rollData.item.auraStrength],
				get label() { return i18n.get('Koboldworks.Missing.PF1.AuraStrength', { strength: this.strengthLabel, school: this.school }); },
			};
		}

		context.size = {
			key: system.size,
			base: pf1.config.actorSizes[system.size],
			get have() { return this.base !== undefined; },
			get label() { return this.base ?? i18n.get('PF1.Undefined'); },
		};

		await this.getDescriptions(context, { showIdentified, canIdentify });

		this._prepareValue(context);
		this._prepareHealth(context);
		this._prepareHardness(context);
		this._prepareWeight(context);
		this._prepareProficiencies(context);
		this._prepareChanges(context);
		await this._prepareActions(context);

		if (system.classSkills) {
			context.skills = Object.entries(system.classSkills)
				.filter(([_, given]) => given)
				.reduce((all, [key]) => {
					all.push({ id: key, label: pf1.config.skills[key] });
					return all;
				}, []);

			context.hasClassSkills = context.skills.length > 0;
		}

		context.hasFlags = Object.values(system.changeFlags ?? {}).some(v => v === true);

		context.hasSystemBFlags = Object.values(system.flags?.boolean ?? {}).length > 0;
		context.hasSystemDFlags = Object.values(system.flags?.dictionary ?? {}).length > 0;
		context.hasSystemFlags = context.hasSystemBFlags || context.hasSystemDFlags;

		context.scriptCalls = item.scriptCalls?.map(s => {
			if (s.type === 'macro') {
				const macro = fromUuidSync(s.value);
				return {
					name: macro?.name || `${i18n.get('Koboldworks.Generic.Missing')} (${i18n.get('DOCUMENT.Macro')})`,
					img: macro?.img || 'icons/svg/hazard.svg',
					invalid: !macro,
					hidden: s.hidden,
				};
			}
			return {
				name: s.name,
				img: s.data.img,
				hidden: s.hidden ?? false,
			};
		}).filter(s => !s.hidden || game.user.isGM);

		context.tag = system.tag || pf1.utils.createTag(this.visibleName);

		return context;
	}

	_prepareValue(context) {
		context.value = getValueInfo(this.item);
	}

	_prepareHealth(context) {
		const system = this.item.system;
		if (system.hp) {
			context.health = {
				value: Number(system.hp.value),
				max: Number(system.hp.max),
				get have() { return Number.isFinite(this.value); }, // PF1 currently stores item HP as string
				get zero() { return this.have && this.max === 0; },
				get inequal() { return this.value !== this.max; },
			};
		}
	}

	_prepareHardness(context) {
		const system = this.item.system;
		context.hardness = {
			value: system.hardness,
			get have() { return Number.isFinite(this.value); },
			get zero() { return this.have && this.value === 0; },
		};
	}

	_prepareWeight(context) {
		const system = this.item.system;
		if (system.weight) {
			context.weight = getWeightInfo(this.item);
		}
	}

	_prepareProficiencies(context) {
		const system = this.item.system;
		const filterProfs = (list) => list?.map(i => i.trim()).filter(i => i.length);

		context.proficiencies = {
			weapon: {
				value: system.weaponProf?.value ?? [],
				custom: filterProfs(system.weaponProf?.custom) ?? [],
				get entries() { return [...this.value.map(p => pf1.config.weaponProficiencies[p]), ...this.custom]; },
				get count() { return this.value.length + this.custom.length; },
			},
			armor: {
				value: system.armorProf?.value ?? [],
				custom: filterProfs(system.armorProf?.custom) ?? [],
				get entries() { return [...this.value.map(p => pf1.config.armorProficiencies[p]), ...this.custom]; },
				get count() { return this.value.length + this.custom.length; },
			},
			languages: {
				value: system.languages?.value ?? [],
				custom: filterProfs(system.langauges?.custom) ?? [],
				get entries() { return [...this.value.map(p => pf1.config.languages[p]), ...this.custom]; },
				get count() { return this.value.length + this.custom.length; },
			},
			get any() { return !!this.weapon.count || !!this.armor.count || !!this.languages.count; },
		};
	}

	_prepareChanges(context) {
		const system = this.item.system,
			buffTargets = getBuffTargets(null);
		context.changes = this.item.changes?.map(ch => ({
			formula: ch.formula,
			value: ch.value,
			operator: operatorLabel[ch.operator],
			type: ch.type,
			typeLabel: pf1.config.bonusTypes[ch.type] || ch.type,
			icon: chIcons[ch.operator],
			hasTarget: !!ch.target,
			targetLabel: buffTargets[ch.target]?.label ?? ch.target,
		})) ?? [];
	}

	async _prepareActions(context) {
		const system = this.item.system;
		if (!(system.actions?.length > 0)) return;

		context.actions = this.item.actions.map(act => ({ ...act.data, raw: act }));

		for (const action of context.actions) {
			this._prepareAction(action, context);
		}

		// Collect all promises
		const promises = context.actions.reduce((total, act) => {
			total.push(act.notes.effect, act.notes.footnotes);

			act.notes.effect?.then(r => act.notes.effect = r);
			act.notes.footnotes?.then(r => act.notes.footnotes = r);
			return total;
		}, []);

		await Promise.all(promises);

		context.actions[0].default = true;
	}

	_prepareAction(actionData, context) {
		const item = this.item,
			system = item.system;

		const minCL = system.cl ?? (system.level > 0 ? (system.level * 2 - 1) : null);

		actionData.labels ??= {};
		const action = actionData.raw;
		const rollData = action.getRollData();

		Object.defineProperties(actionData, {
			id: {
				value: actionData._id,
			},
			hasAttack: {
				value: ['mwak', 'rwak', 'msak', 'rsak', 'mcman', 'rcman'].includes(actionData.actionType),
			},
			hasDamage: {
				get() { return this.raw.hasDamage; },
			},
			hasSave: {
				value: !!actionData.save?.type,
			},
			hasRange: {
				value: !!actionData.range?.units && actionData.range?.units !== 'none',
			},
			hasCritical: {
				value: actionData.ability?.critRange > 0 && actionData.ability?.critMult > 0,
			},
		});

		// Activation
		const unchained = game.settings.get('pf1', 'unchainedActionEconomy'),
			act = action.activation,
			tr1 = unchained ? pf1.config.abilityActivationTypes_unchained : pf1.config.abilityActivationTypes,
			trM = unchained ? pf1.config.abilityActivationTypesPlurals_unchained : pf1.config.abilityActivationTypesPlurals;
		let activation;
		act.cost ||= 1;
		if (act.cost <= 1) activation = tr1[act.type] || i18n.get('PF1.Undefined');
		else activation = `${act.cost} ${trM[act.type] || i18n.get('PF1.Undefined')}`;
		actionData.labels.activation = activation;

		// Duration
		const du = actionData.duration?.units;
		if (du) {
			actionData.labels.duration = {};
			const units = pf1.config.timePeriods[du];
			if (du === 'spec') {
				actionData.labels.duration.static = true;
				actionData.labels.duration.label = actionData.duration.value;
			}
			else if (!['perm', 'inst', 'seeText'].includes(du)) {
				const formula = actionData.duration.value;
				const roll = safeEval(formula || '0', rollData);
				actionData.labels.duration.formula = formula;
				actionData.labels.duration.resolved = true;
				actionData.labels.duration.label = `${roll.total} ${units}`;
			}
			else {
				if (this.item.type !== 'spell' && du === 'inst') {
					// NOP
				}
				else {
					actionData.labels.duration.static = true;
					actionData.labels.duration.label = units;
				}
			}
		}

		// Range
		if (actionData.hasRange) {
			actionData.labels.range = {};
			const ru = actionData.range?.units;
			let range = pf1.config.distanceUnits[ru];
			let tip;
			switch (ru) {
				case 'spec':
					range = actionData.range.value || range;
					break;
				case 'long':
				case 'medium':
				case 'close':
					actionData.labels.range.aux = action.getRange('single');
					break;
				case 'mi': {
					const roll = safeEval(actionData.range.value || '0', rollData);
					range = `${roll.total} ${range}`;
					break;
				}
				case 'ft': {
					const r = action.getRange();
					range = `${r} ${context.distanceUnit}`;

					// TODO: Max range
					// console.log(actionData.range);
					const maxIncr = actionData.range.maxIncrements || 1;
					actionData.labels.range.incr = maxIncr;
					if (maxIncr > 1) {
						actionData.labels.range.max = `${action.maxRange} ${context.distanceUnit}`;
					}
					break;
				}
				/*
				case 'personal':
				case 'seeText':
				case 'unlimited':
				case 'touch':
				case 'melee':
				case 'reach':
				*/
				default:
					break;
			}

			actionData.labels.range.label = range;

			switch (ru) {
				case 'long':
					tip = '400 + 40 × CL';
					break;
				case 'medium':
					tip = '100 + 10 × CL';
					break;
				case 'short':
					tip = '25 + 5 × CL/2';
					break;
				case 'mi':
				case 'ft':
					tip = actionData.range.value;
					break;
			}
			actionData.labels.range.tip = tip;
		}

		if (actionData.hasAttack) {
			// Bonus
			const formula = actionData.labels.bonusFormula = actionData.attackBonus,
				broll = safeEval(formula || '0', rollData),
				bonus = broll.total ?? 0;

			const famformula = actionData.labels.famFormula = actionData.formulaicAttacks?.bonus?.formula,
				famroll = safeEval(famformula || '0', rollData),
				fambonus = famroll.total ?? 0;
			actionData.totalBonus = bonus + fambonus;

			actionData.labels.abilityBonus = pf1.config.abilitiesShort[actionData.ability?.attack];

			// Critical
			if (actionData.hasCritical) {
				const cr = actionData.ability?.critRange;
				actionData.labels.critRange = cr === 20 ? '20' : `${cr}–20`;
				actionData.labels.critMult = `${actionData.ability?.critMult}`;
			}

			// Attack Count
			// TODO: REDO
			/*
			const extraAttacks = (actionData.attackParts?.length ?? 0),
				famattacks = actionData.formulaicAttacks?.count?.formula;
			actionData.labels.attacksFormula = famattacks?.length > 0 ? `1[Base] + ${extraAttacks}[Extra] + ${famattacks}` : `1[Base] + ${extraAttacks}[Extra]`;
			const fatrd = this.rollData;

			fatrd.cl = minCL;
			fatrd.sl = system.level ?? 1;
			setProperty(fatrd, 'attributes.bab.total', 1);
			const fat0roll = safeEval(famattacks || '0', fatrd);
			const fat0 = fat0roll.total ?? 0;
			fatrd.cl = system.cl ?? 20;
			fatrd.attributes.bab.total = 20;
			const fat1roll = safeEval(famattacks || '0', fatrd);
			const fat1 = fat1roll.total ?? 0;
			delete fatrd.cl;
			delete fatrd.sl;
			delete fatrd.attributes.bab.total;

			const minAttacks = 1 + extraAttacks + (fat0 ?? 0),
				maxAttacks = 1 + extraAttacks + (fat1 ?? 0);
			if (minAttacks !== maxAttacks && Number.isFinite(maxAttacks))
				actionData.labels.attacks = `${minAttacks} – ${maxAttacks}`;
			else
				actionData.labels.attacks = `${minAttacks}`;
			*/
		}

		if (actionData.hasDamage) {
			actionData.isHealing = actionData.actionType === 'heal';

			actionData.labels.abilityDamage = pf1.config.abilitiesShort[actionData.ability?.damage];
			const dp = {};
			const getTypeLabels = (t) => {
				if (!t) return [pf1.registry.damageTypes.get('untyped').name];
				const types = t.values.map(tt => {
					const type = pf1.registry.damageTypes.get(tt);
					if (!type) return tt;
					if (type.abbr) {
						if (i18n.has(type.abbr)) return i18n.get(type.abbr);
						return type.abbr;
					}
					return type.name;
				});
				if (types.custom) types.push(t.custom);
				return types;
			};

			rollData.size = 4; // Default size
			rollData.cl = system.cl ?? minCL;
			rollData.sl = system.level ?? 1;
			for (const { formula, type } of actionData.damage.parts) {
				const basetype = type?.values.join('+') || type?.custom || 'untyped';
				dp[basetype] ??= { instances: [], formulas: [], type, labels: getTypeLabels(type).join('/') };
				const data = dp[basetype];
				// Simple sizeRoll mapper
				data.formulas.push(formula);
				const sformula = pf1.utils.formula.simplify(formula, rollData, { strict: false });
				const roll = safeEval(sformula, rollData);
				data.instances.push(roll.formula);
			}
			delete rollData.cl;
			delete rollData.sl;
			Object.entries(dp).forEach(([key, value]) => {
				value.total = value.instances.join(' + ');
				value.formula = value.formulas.join(' + ');
			});
			actionData.damageInfo = dp;

			// Ability bonus
			if (actionData.ability?.damage) {
				const isNaturalPrimary = actionData.naturalAttack?.primaryAttack ?? true;
				actionData.abilityDamage = {
					key: actionData.ability.damage,
					mult: isNaturalPrimary ? (actionData.ability.damageMult ?? 1) : actionData.naturalAttack?.secondary?.damageMult ?? 1,
				};
			}
		}

		if (actionData.hasSave) {
			actionData.labels.saveType = pf1.config.savingThrows[actionData.save.type];
			actionData.labels.saveDesc = actionData.save.description;

			const baseFormula = actionData.save?.dc || '',
				broll = safeEval(baseFormula || '0', rollData),
				bonus = broll.total ?? 0;

			const level = system.level ?? 1;
			if (item.type === 'spell') {
				const abl = Math.floor(level / 2),
					dc = 10 + level + Math.floor(level / 2) + bonus;
				actionData.labels.saveDC = dc;
				const slLabel = i18n.get('PF1.SpellLevel'),
					ablLabel = i18n.get('PF1.Ability');
				actionData.labels.saveFormula = `10 + ${level}[${slLabel}] + ${abl}[${ablLabel}]`;
				if (baseFormula.length && baseFormula !== '0')
					actionData.labels.saveFormula += ` + ${baseFormula}`;
			}
			else {
				actionData.labels.saveDC = bonus;
				actionData.labels.saveFormula = baseFormula;
			}
		}

		// Ammunition
		if (actionData.usesAmmo) {
			const ammo = item.actor?.items.get(item.getFlag('pf1', 'defaultAmmo'));
			context.ammunition = {
				type: actionData.ammoType,
				linked: !!ammo,
				name: ammo?.name,
				img: ammo?.img,
				quantity: ammo?.system.quantity ?? 0,
			};
		}

		actionData.notes ??= {};
		if (actionData.effectNotes?.length) {
			actionData.notes.effect = renderTemplate(flatTemplates.itemNotes, { notes: actionData.effectNotes })
				.then(n => TextEditor.enrichHTML(n, { rollData, async: true }));
		}
		if (actionData.attackNotes?.length) {
			actionData.notes.footnotes = renderTemplate(flatTemplates.itemNotes, { notes: actionData.attackNotes })
				.then(n => TextEditor.enrichHTML(n, { rollData, async: true }));
		}
	}

	_editSheet;
	/**
	 * @param {JQuery} jq
	 */
	activateListeners(jq) {
		super.activateListeners(jq);

		const html = jq[0];

		const controls = html.querySelector('.controls');
		const editSheet = controls.querySelector('.edit-sheet');
		editSheet?.addEventListener('click', (ev) => {
			ev.preventDefault();
			const sheetOptions = {};
			const isContainer = this.item.type === 'container';
			const cls = isContainer ? pf1.applications.item.ItemSheetPF_Container : pf1.applications.item.ItemSheetPF;
			this._editSheet ??= new cls(this.item, sheetOptions);
			this._editSheet.render(true, { focus: true });
		});

		// Tooltip target URLs
		html.querySelector('.description')?.querySelectorAll('a[href]')?.forEach(el => {
			el.dataset.tooltip = el.href;
		});
	}

	close() {
		this._editSheet = null;
		super.close();
	}
}
