import { CFG } from '@root/config.mjs';
import { InfoSheet } from '../infosheet.mjs';

import { getFormulaVariables, resolveFormula } from '@root/utility/formula.mjs';

const timeMult = {
	turn: 6,
	round: 6,
	minute: 60,
	hour: 60 * 60,
};

const timeLabel = {
	turn: 'Koboldworks.Time.Round',
	round: 'Koboldworks.Time.Round',
	minute: 'Koboldworks.Time.Minutes',
	hour: 'Koboldworks.Time.Hours',
	permanent: 'PF1.Permanent',
};

export class InfoSheetBuff extends InfoSheet {
	async getData() {
		const item = this.item,
			system = item.system,
			rollData = this.rollData;
		const context = await super.getData();
		const subtype = context.subType = item.subType;
		context.labels.type = pf1.config.buffTypes[subtype];

		context.hasLevel = Number.isFinite(system.level);

		const { value: formula, units } = system.duration;

		if (rollData?.item?.level === undefined)
			setProperty(rollData, 'item.level', system.level);

		const dur = units ? await resolveFormula(formula, rollData) : null;

		context.labels.duration = {
			permanent: !units,
			formula,
			value: dur?.total,
			resolved: Number.isFinite(dur?.total),
			units: timeLabel[units] || timeLabel.permanent,
			endOn: units ? pf1.config.durationEndEvents[dur?.end || 'turnStart'] : null,
		};

		return context;
	}
}

Object.defineProperty(CFG.sheetClasses.infosheet, 'buff', {
	get: () => InfoSheetBuff,
	enumerable: true,
});
