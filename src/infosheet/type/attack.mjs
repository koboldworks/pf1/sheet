import { CFG } from '../../config.mjs';
import { i18n } from '../../utility/i18n.mjs';
import { InfoSheet } from '../infosheet.mjs';

export class InfoSheetAttack extends InfoSheet {
	async getData() {
		const item = this.item,
			system = item.system;
		const context = await super.getData();

		context.labels.type = pf1.config.attackTypes[item.subType];

		context.isPhysical = ['weapon', 'item'].includes(item.subtype);
		context.isNatural = item.subType === 'natural';

		const isMasterwork = system.masterwork === true,
			isBroken = system.broken === true,
			isProficient = system.proficient === true;
		context.isProficient = isProficient;
		context.hasProficiency = true;
		context.isMasterwork = isMasterwork;
		context.hasMasterwork = context.isPhysical;
		context.isBroken = isBroken;
		context.hasBroken = context.isPhysical;

		context.hasQualities = context.isPhysical;

		const enhancement = system.enh ?? 0;
		context.enhancement = {
			bonus: enhancement,
			get have() { return this.bonus > 0; },
		};

		return context;
	}
}

Object.defineProperty(CFG.sheetClasses.infosheet, 'attack', {
	get: () => InfoSheetAttack,
	enumerable: true,
});
