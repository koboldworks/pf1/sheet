import { CFG } from '@root/config.mjs';
import { i18n } from '@root/utility/i18n.mjs';
import { InfoSheetPhysical } from './physical.mjs';

import { getVisibleName, getWeightInfo, getValueInfo } from '@root/utility/item.mjs';

export class InfoSheetContainer extends InfoSheetPhysical {
	async getData() {
		const item = this.item,
			system = item.system;
		const context = await super.getData();

		this._prepareContents(context);

		context.weight.haveContents = true;
		context.weight.contents = system.weight.converted.contents ?? 0;

		if (system.weightReduction > 0) {
			context.weight.hasReduction = true;
			context.weight.reduction = system.weightReduction;
		}

		return context;
	}

	_prepareContents(context) {
		context.items = this.item.items.map(i => ({
			id: i.id,
			name: getVisibleName(i),
			type: i.type,
			img: i.img,
			sort: i.sort,
			value: getValueInfo(i),
			weight: getWeightInfo(i),
			quantity: i.system.quantity ?? 1,
			system: i.system,
			document: i,
		}))
			.sort((a, b) => b.sort - a.sort);

		context.itemsByCategory = [];
		const categories = {};
		['weapon', 'equipment', 'consumable', 'loot', 'container', 'other']
			.forEach(type => {
				let label = pf1.config.itemTypes[type];
				if (!label) {
					if (type === 'container') label = 'ITEM.TypeContainer';
					else label = 'PF1.Other';
				}
				const d = categories[type] = { type, label, items: [] };
				context.itemsByCategory.push(d);
			});
		context.items.forEach(item => {
			const cd = categories[item.type] ?? categories.other;
			cd.items.push(item);
		});
	}

	/**
	 * @param {Event} ev
	 * @param {string} itemId
	 */
	_openItem(ev, itemId) {
		const item = this.item.items.get(itemId);

		item.sheet.render(true, { focus: true });
	}

	/**
	 * @param {JQuery} jq
	 */
	activateListeners(jq) {
		super.activateListeners(jq);
		const html = jq[0];

		const app = this;

		function openSheetProxy(ev) {
			const itemId = this.dataset.itemId;
			app._openItem(ev, itemId);
		}

		html.querySelectorAll('.item-list .item[data-item-id]')
			.forEach(el => el.addEventListener('contextmenu', openSheetProxy));
	}
}

Object.defineProperty(CFG.sheetClasses.infosheet, 'container', {
	get: () => InfoSheetContainer,
	enumerable: true,
});
