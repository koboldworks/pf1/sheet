import { CFG } from '@root/config.mjs';
import { i18n } from '@root/utility/i18n.mjs';
import { InfoSheet } from '../infosheet.mjs';

import { resolveFormula } from '@root/utility/formula.mjs';
import { prettyNumber } from '@root/utility/numbers.mjs';

export class InfoSheetClass extends InfoSheet {
	async getData() {
		const item = this.item,
			system = item.system;
		const context = await super.getData();
		const subtype = context.subtype = item.subType;
		context.labels.type = pf1.config.classTypes[subtype];

		// TODO: Add mythic path handling
		context.inPack = !!item.pack;

		context.level = system.level;
		context.haveLevel = subtype !== 'racial';
		context.labels.level = subtype !== 'mythic' ? 'PF1.Level' : 'PF1.Tier';
		context.hd = item.hitDice,
		context.isLeveled = system.level >= 1;
		if (context.inPack) context.isLeveled = false;
		context.hasLevels = context.isLeveled && system.level > 1;

		context.isMythic = item.subType === 'mythic';

		if (context.isMythic) {
			context.healthPerTier = {
				per: system.hd,
				tier: system.level,
				have: system.level > 0,
			};
		}
		else {
			context.hitDice = {
				die: system.hd,
				dice: context.hd,
				have: context.hd > 0,
			};
		}

		context.health = {
			total: system.hp,
			average: system.hp / system.level,
		};

		context.bab = {
			type: system.bab,
			base: system.babBase,
			get prettyTotal() { return prettyNumber(this.base); },
			label: pf1.config.classBAB[system.bab] ?? i18n.get('PF1.None'),
		};

		context.skillsTotal = system.skillsPerLevel * context.hd;

		// TODO: Handle good saves with fractional saves optional rule

		context.saves = {};

		['fort', 'ref', 'will'].forEach(saveId => {
			let total = system.savingThrows[saveId].base;
			if (system.savingThrows[saveId].good) {
				total += RollPF.safeRollSync(pf1.config.classFractionalSavingThrowFormulas.goodSaveBonus).total;
			}
			context.saves[saveId] = {
				label: pf1.config.classSavingThrows[system.savingThrows[saveId].value],
				total,
				get prettyTotal() { return prettyNumber(this.total); },
				get have() { return !!this.label; },
			};
		});

		return context;
	}
}

Object.defineProperty(CFG.sheetClasses.infosheet, 'class', {
	get: () => InfoSheetClass,
	enumerable: true,
});
