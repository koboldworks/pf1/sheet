import { CFG } from '../../config.mjs';
import { i18n } from '../../utility/i18n.mjs';
import { InfoSheetPhysical } from './physical.mjs';

export class InfoSheetLoot extends InfoSheetPhysical {
	get isAmmo() {
		return this.item.system.subType === 'ammo';
	}

	async getData() {
		const item = this.item,
			system = item.system;
		const context = await super.getData();
		const subtype = context.subtype = item.subType;
		context.labels.type = pf1.config.lootTypes[subtype];

		const isAmmo = context.isAmmo = this.isAmmo;
		if (isAmmo) {
			context.ammoType = system.extraType;
			context.ammoTypeUnset = !context.ammoType;
			context.labels.ammo = pf1.config.ammoTypes[context.ammoType] || i18n.get('PF1.Unknown');
			context.abundant = item.flags?.pf1?.abundant ?? false;
		}

		return context;
	}
}

Object.defineProperty(CFG.sheetClasses.infosheet, 'loot', {
	get: () => InfoSheetLoot,
	enumerable: true,
});
