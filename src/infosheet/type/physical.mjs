import { i18n } from '@root/utility/i18n.mjs';
import { InfoSheet } from '../infosheet.mjs';

export class InfoSheetPhysical extends InfoSheet {
	async getData() {
		const material = this.item.normalMaterial;
		return {
			...await super.getData(),
			isPhysical: true,
			material: {
				have: !!material,
				value: pf1.registry.materialTypes.get(material) ?? { name: material || i18n.get('PF1.None') },
				addons: this.item.addonMaterial?.map(m => pf1.registry.materialTypes.get(m) || { name: m, invalid: true }),
			},
		};
	}
}
