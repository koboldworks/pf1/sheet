import { CFG } from '../../config.mjs';
import { i18n } from '../../utility/i18n.mjs';
import { InfoSheet } from '../infosheet.mjs';

import { safeEval } from '@root/utility/roll.mjs';

export class InfoSheetFeat extends InfoSheet {
	async getData() {
		const item = this.item,
			system = item.system;
		const context = await super.getData();

		const isTemplate = system.featType === 'template';
		context.hasSubType = !isTemplate;
		const subtype = context.subtype = item.subType;

		context.labels.type = pf1.config.featTypes[subtype];

		if (isTemplate) {
			const formula = system.crOffset ?? '',
				roll = safeEval(formula, this.rollData);

			context.CR = {
				formula,
				value: roll.total,
				have: formula != 0,
			};
		}

		return context;
	}
}

Object.defineProperty(CFG.sheetClasses.infosheet, 'feat', {
	get: () => InfoSheetFeat,
	enumerable: true,
});
