import { CFG } from '../../config.mjs';
import { i18n } from '../../utility/i18n.mjs';
import { InfoSheetPhysical } from './physical.mjs';

export class InfoSheetConsumable extends InfoSheetPhysical {
	async getData() {
		const item = this.item,
			system = item.system;
		const context = await super.getData();

		const subtype = context.subtype = item.subType;
		context.labels.type = pf1.config.consumableTypes[subtype];

		return context;
	}
}

Object.defineProperty(CFG.sheetClasses.infosheet, 'consumable', {
	get: () => InfoSheetConsumable,
	enumerable: true,
});
