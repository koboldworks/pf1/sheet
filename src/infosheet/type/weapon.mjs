import { CFG } from '../../config.mjs';
import { i18n } from '../../utility/i18n.mjs';
import { InfoSheetPhysical } from './physical.mjs';

export class InfoSheetWeapon extends InfoSheetPhysical {
	async getData() {
		const item = this.item,
			system = item.system;
		const context = await super.getData();

		const subtype = context.subtype = item.subType;
		const category = context.category = system.weaponSubtype;
		const types = pf1.config.weaponTypes[subtype];
		context.labels.type = types?._label;
		context.labels.category = types[category] ?? i18n.get('PF1.Undefined');

		const { isIdentified } = context;

		context.enhancement = {
			bonus: isIdentified ? (system.enh ?? 0) : 0,
			get have() { return this.bonus > 0; },
		};

		const isMasterwork = system.masterwork === true,
			isBroken = system.broken === true;
		context.isMasterwork = isMasterwork;
		context.hasMasterwork = true;
		context.isBroken = isBroken;
		context.hasQualities = true;

		context.twoHanded = true;
		context.weaponGroups = [];
		context.isRanged = true;
		context.properties = [];

		this._prepareProperties(context);
		this._prepareWeaponGroups(context);

		return context;
	}

	_prepareProperties(context) {
		const properties = [];
		for (const [key, have] of Object.entries(this.item.system.properties))
			if (have) properties.push(pf1.config.weaponProperties[key]);

		properties.sort((a, b) => a.localeCompare(b));

		context.properties = properties;
	}

	_prepareWeaponGroups(context) {
		const groups = this.item.system.weaponGroups;

		const weaponGroups = [];
		groups.value.forEach(key => weaponGroups.push(pf1.config.weaponGroups[key]));
		if (Array.isArray(groups.custom))
			groups.custom.forEach(v => weaponGroups.push(v));
		else
			groups.custom?.split(';').map(v => v.trim()).filter(i => !!i).forEach(v => weaponGroups.push(v));

		weaponGroups.sort((a, b) => a.localeCompare(b));

		context.weaponGroups = weaponGroups;
	}
}

Object.defineProperty(CFG.sheetClasses.infosheet, 'weapon', {
	get: () => InfoSheetWeapon,
	enumerable: true,
});
