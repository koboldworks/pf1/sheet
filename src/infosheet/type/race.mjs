import { CFG } from '../../config.mjs';
import { i18n } from '../../utility/i18n.mjs';
import { InfoSheet } from '../infosheet.mjs';

export class InfoSheetRace extends InfoSheet {
	async getData() {
		const item = this.item,
			system = item.system;
		const context = await super.getData();
		context.labels.type = pf1.config.creatureTypes[system.creatureType];

		context.subTypes = system.subTypes?.flat() ?? [];

		return context;
	}
}

Object.defineProperty(CFG.sheetClasses.infosheet, 'race', {
	get: () => InfoSheetRace,
	enumerable: true,
});
