import { CFG } from '../../config.mjs';
import { i18n } from '../../utility/i18n.mjs';
import { InfoSheet } from '../infosheet.mjs';

export class InfoSheetSpell extends InfoSheet {
	async getData() {
		const item = this.item,
			system = item.system;

		const context = await super.getData();

		const subtype = context.subtype = item.subType;
		context.labels.type = pf1.config.consumableTypes[subtype];

		// const rollData = this.rollData;

		const nullEmpty = (obj) => foundry.utils.isEmpty(obj) ? null : obj;

		context.spell = {
			school: pf1.config.spellSchools[system.school] || 'PF1.Undefined',
			subschool: system.subschool,
			types: system.types,
			learnedAt: {
				get any() { return (this.class.entries || this.domain.entries || this.subdomain.entries || this.elemental.entries || this.bloodline.entries); },
				class: {
					label: 'PF1.Level',
					entries: nullEmpty(system.learnedAt.class),
				},
				domain: {
					label: 'PF1.Domain',
					entries: nullEmpty(system.learnedAt.domain),
				},
				subdomain: {
					label: 'PF1.SubDomain',
					entries: nullEmpty(system.learnedAt.subDomain),
				},
				elemental: {
					label: 'PF1.ElementalSchool',
					entries: nullEmpty(system.learnedAt.elementalSchool),
				},
				bloodline: {
					label: 'PF1.Bloodline',
					entries: nullEmpty(system.learnedAt.bloodline),
				},
			},
			resistance: system.sr,
			components: null,
			focus: {
				get have() { return this.item.length > 0; },
				item: system.materials.focus,
				// value: // system doesn't provide this
			},
			materials: {
				get have() { return this.item.length > 0; },
				item: system.materials.value,
				value: system.materials.gpValue,
			},
		};

		context.spell.components = this.item.getSpellComponents({ compact: true });

		return context;
	}
}

Object.defineProperty(CFG.sheetClasses.infosheet, 'spell', {
	get: () => InfoSheetSpell,
	enumerable: true,
});
