import { CFG } from '../../config.mjs';
import { i18n } from '../../utility/i18n.mjs';
import { InfoSheetPhysical } from './physical.mjs';

export class InfoSheetEquipment extends InfoSheetPhysical {
	async getData() {
		const item = this.item,
			system = item.system;

		const context = await super.getData();

		const subtype = item.subType;
		const category = system.equipmentSubtype;
		// context.labels.type = pf1.config.equipmentTypes[system.equipmentType]?._label ?? 'PF1.Undefined';
		const eqLabels = pf1.config.equipmentTypes[subtype];
		context.labels.type = eqLabels?.[category] ?? eqLabels?._label ?? 'PF1.Undefined';

		context.isArmor = subtype === 'armor';
		context.isShield = subtype === 'shield';
		context.isClothing = subtype === 'clothing';
		const isMisc = context.isMisc = subtype === 'misc';

		// Hide material for clothes which do not currently support it
		if (context.isClothing) context.material = null;

		context.labels.slot = context.isMisc ? pf1.config.equipmentSlots[subtype]?.[system.slot] : null;

		if (!isMisc) this._prepareArmor(context);

		return context;
	}

	_prepareArmor(context) {
		const system = this.item.system;

		const { isIdentified } = context;

		const enhancement = isIdentified ? (system.armor?.enh ?? 0) : 0;
		context.enhancement = {
			bonus: enhancement,
			get have() { return this.bonus > 0; },
		};

		const isMasterwork = system.masterwork === true,
			isBroken = system.broken === true;
		context.isMasterwork = isMasterwork;
		context.hasMasterwork = true;
		context.isBroken = isBroken;
		context.hasQualities = true;

		context.armor = {
			base: system.armor?.value ?? 0,
			enh: enhancement,
			get combined() { return this.base + this.enh; },
			total: 0,
			isBroken,
			get brokenTotal() { return this.isBroken ? Math.floor(this.total / 2) : this.total; },
			get mismatch() { return this.total !== this.base; },
			get have() { return this.combined > 0; },
		};
		context.armor.total = context.armor.combined;

		context.acp = {
			base: system.armor?.acp ?? 0,
			total: 0,
			get have() { return this.total > 0; },
			isMasterwork,
			get isMasterworkAdjusted() { return this.base > 0 && this.isMasterwork; },
			get mismatch() { return this.total !== this.base; },
		};
		context.acp.total = context.acp.base;
		if (isMasterwork && context.acp.total > 0)
			context.acp.total -= 1;

		context.maxDex = {
			limit: system.armor?.dex ?? null,
			have: Number.isFinite(system.armor?.dex),
		};

		context.asf = {
			failure: system.spellFailure ?? 0,
			get have() { return this.failure > 0; },
			get label() { return `${this.failure}%`; },
		};

		context.isArmor = context.armor.combined > 0 || context.acp.base > 0 || context.maxDex.have || context.asf.failure > 0;
	}
}

Object.defineProperty(CFG.sheetClasses.infosheet, 'equipment', {
	get: () => InfoSheetEquipment,
	enumerable: true,
});
