// Partial copy from PF1 source to deal with it missing from exposed API
// License: GPLv3 <https://gitlab.com/foundryvtt_pathfinder1e/foundryvtt-pathfinder1/-/blob/master/LICENSE.txt>

export const getBuffTargets = () => {
	const buffTargets = duplicate(pf1.config.buffTargets);

	for (const [k, v] of Object.entries(pf1.config.skills))
		buffTargets[`skill.${k}`] = { label: v, category: 'skill' };

	return buffTargets;
};
