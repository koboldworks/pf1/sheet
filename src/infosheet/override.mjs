import { CFG } from '../config.mjs';
import { InfoSheet } from './infosheet.mjs';

/* global libWrapper */

function softOverrideDefaultSheet(wrapped) {
	if (this.actor || !InfoSheet.SUPPORTED_TYPES.includes(this.type))
		return wrapped.call(this);

	// Partial copy of Foundry's base implementation
	const type = this.type ?? CONST.BASE_DOCUMENT_TYPE;
	const sheets = CONFIG[this.documentName].sheetClasses[type] || {};

	const override = this.getFlag('core', 'sheetClass');
	if (sheets[override]) return wrapped.call(this);

	return InfoSheet;
}

const preCreateItemOnActor = (item) => {
	const override = item.getFlag('core', 'sheetClass');
	if (!!item.actor && override === 'Koboldworks.InfoSheet') {
		if (game.settings.get(CFG.id, CFG.SETTINGS.stripInfoSheet)) {
			item.updateSource({ flags: { core: { '-=sheetClass': null } } });
		}
	}
}

const registerSheetOverride = () => {
	libWrapper.register(CFG.id, 'Item.prototype._getSheetClass', softOverrideDefaultSheet, libWrapper.MIXED);
}

Hooks.on('preCreateItem', preCreateItemOnActor);
Hooks.once('init', () => game.settings.get(CFG.id, CFG.SETTINGS.defaultInfoSheet) ? registerSheetOverride() : null);
