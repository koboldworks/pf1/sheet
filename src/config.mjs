export const CFG = /** @type {const} */ ({
	id: 'koboldworks-pf1-sheet',
	SETTINGS: {
		stripInfoSheet: 'removeInfoSheet',
		defaultInfoSheet: 'defaultInfoSheet',
	},
	sheetClasses: {
		statsheet: {

		},
		spellbook: {

		},
		datasheet: {

		},
		infosheet: {

		},
	},
});

const getTemplatePath = (/** @type {string} */ file) => /** @type {const} */ (`modules/${CFG.id}/template/${file}.hbs`);

export const templates = {
	spellbook: getTemplatePath('spellbook'),
	datasheet: getTemplatePath('datasheet'),
	infosheet: {
		type: {
			weapon: getTemplatePath('sub/infosheet/weapon'),
			equipment: getTemplatePath('sub/infosheet/equipment'),
			consumable: getTemplatePath('sub/infosheet/consumable'),
			loot: getTemplatePath('sub/infosheet/loot'),
			class: getTemplatePath('sub/infosheet/class'),
			spell: getTemplatePath('sub/infosheet/spell'),
			feat: getTemplatePath('sub/infosheet/feat'),
			buff: getTemplatePath('sub/infosheet/buff'),
			attack: getTemplatePath('sub/infosheet/attack'),
			race: getTemplatePath('sub/infosheet/race'),
			// container: null
		},
		sub: {
			changes: getTemplatePath('sub/infosheet/part/changes'),
			description: getTemplatePath('sub/infosheet/part/description'),
			proficiencies: getTemplatePath('sub/infosheet/part/proficiencies'),
			header: getTemplatePath('sub/infosheet/part/header'),
			footer: getTemplatePath('sub/infosheet/part/footer'),
			actions: getTemplatePath('sub/infosheet/part/actions'),
			enhancement: getTemplatePath('sub/infosheet/part/enhancement'),
			physical: getTemplatePath('sub/infosheet/part/physical'),
			qualities: getTemplatePath('sub/infosheet/part/qualities'),
			groups: getTemplatePath('sub/infosheet/part/groups'),
			properties: getTemplatePath('sub/infosheet/part/properties'),
			armor: getTemplatePath('sub/infosheet/part/armor'),
			skills: getTemplatePath('sub/infosheet/part/skills'),
			class: getTemplatePath('sub/infosheet/part/class'),
			container: getTemplatePath('sub/infosheet/part/container'),
			components: getTemplatePath('sub/infosheet/part/components'),
			learnedAt: getTemplatePath('sub/infosheet/part/learned-at'),
			flags: getTemplatePath('sub/infosheet/part/flags'),
			systemflags: getTemplatePath('sub/infosheet/part/system-flags'),
			scripts: getTemplatePath('sub/infosheet/part/scripts'),
			tags: getTemplatePath('sub/infosheet/part/tags'),
		},
	},
	statblock: {
		item: {
			spell: getTemplatePath('sub/statblock/item-spell'),
			ability: getTemplatePath('sub/statblock/item-ability'),
			attack: getTemplatePath('sub/statblock/item-attack'),
			showAll: getTemplatePath('sub/statblock/show-all'),
		},
		section: {
			spells: getTemplatePath('sub/statblock/section-spells'),
			stats: getTemplatePath('sub/statblock/section-stats'),
			equipment: getTemplatePath('sub/statblock/section-equipment'),
			misc: getTemplatePath('sub/statblock/section-misc'),
			offense: getTemplatePath('sub/statblock/section-offense'),
			defense: getTemplatePath('sub/statblock/section-defense'),
			details: getTemplatePath('sub/statblock/section-details'),
		},
		/*
		spellbook: {
			components: getTemplatePath('spellbook/components'),
		}
		*/
	},
	helpers: {
		checkbox: getTemplatePath('helpers/checkbox'),
	},
	dialog: {
		value: getTemplatePath('dialog/value'),
		skill: getTemplatePath('dialog/skill'),
	},
};

// Templates for use with renderTemplate()
export const /** @type {const} */ flatTemplates = ({
	itemNotes: getTemplatePath('sub/infosheet/part/notes'),
	statsheet: getTemplatePath('statsheet'),
});

const generateTemplateKey = (/** @type {string} */ key) => 'koboldworks-' + key.replace(/\./g, '-');

// Preload handlebars templates
Hooks.once('setup', () => {
	// Named templates
	const t = {};
	Object.entries(flattenObject(templates))
		.forEach(([key, value]) => t[generateTemplateKey(key)] = value);
	Promise.resolve().then(() => loadTemplates(t));

	// Flat templates
	Promise.resolve().then(() => loadTemplates(Object.values(flatTemplates)));
});

// Since Foundry does not support hot reloading object notation templates...
Hooks.on('hotReload', async ({ content, extension, packageId, packageType, path } = {}) => {
	if (packageId !== CFG.id) return;
	if (extension === 'hbs') {
		const key = Object.entries(flattenObject(templates)).find(([_, tpath]) => tpath == path)?.[0];
		if (!key) throw new Error(`Unrecognized template: ${path}`);

		await new Promise((resolve, reject) => {
			game.socket.emit('template', path, resp => {
				if (resp.error) return reject(new Error(resp.error));
				const compiled = Handlebars.compile(resp.html);
				Handlebars.registerPartial(generateTemplateKey(key), compiled);
				console.log(`Foundry VTT | Retrieved and compiled template ${path} as ${key}`);
				resolve(compiled);
			});
		});

		Object.values(ui.windows).forEach(app => app.render(true));
	}
});
