import { CFG } from '../config.mjs';

export class ConfigureAbilities extends DocumentSheet {
	get title() {
		return `Configure Ability Links: ${this.document.name}`;
	}

	get template() {
		return `modules/${CFG.id}/template/config/abilities.hbs`;
	}

	static get defaultOptions() {
		const _defaults = super.defaultOptions;
		return mergeObject(_defaults, {
			classes: [..._defaults.classes, 'koboldworks', 'ability-config'],
			width: 'auto',
			submitOnChange: true,
			submitOnClose: true,
			closeOnSubmit: false,
			// resizable: true,
		});
	}

	getData() {
		const data = super.getData();
		const actor = data.actor = this.document;
		data.system = actor.system;
		data.config = pf1.config;
		return data;
	}
}
