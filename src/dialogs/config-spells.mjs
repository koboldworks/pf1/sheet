import { CFG } from '../config.mjs';

export class ConfigureSpells extends DocumentSheet {
	get title() {
		return `Configure Spellcasting: ${this.document.name}`;
	}

	get template() {
		return `modules/${CFG.id}/template/config/spells.hbs`;
	}

	static get defaultOptions() {
		const _defaults = super.defaultOptions;
		return mergeObject(_defaults, {
			title: 'Configure Spellcasting',
			classes: [..._defaults.classes, 'koboldworks', 'spell-config'],
			tabs: [
				{ navSelector: 'nav.tabs', contentSelector: 'content.tabs' },
			],
			scrollY: ['content.tabs section'],
			submitOnChange: true,
			submitOnClose: true,
			closeOnSubmit: false,
			resizable: true,
		});
	}

	getData() {
		const data = super.getData();
		const actor = data.actor = this.document;
		const actorData = actor.toObject();
		data.uuid = actor.uuid;
		// const rollData = data.rollData = actor.getRollData();
		data.system = actor.system;
		// Classes
		data.classes = actor.items
			.filter(i => i.type === 'class')
			.map(cls => ({ name: cls.name, tag: cls.system.tag, hitDice: cls.hitDice }));
		// Spellbooks
		data.books = actorData.system.attributes?.spells?.spellbooks ?? {};
		const derivedBookData = actor.system.attributes?.spells?.spellbooks;

		Object.entries(data.books).forEach(([key, data]) => {
			const derived = derivedBookData[key];
			data.label = derived.label;
			data.concentration = derived.concentration;
			data.range = derived.range;
			data.spells = derived.spells;
		});

		data.castingProgression = {
			low: 'PF1.Low',
			med: 'PF1.Medium',
			high: 'PF1.High'
		};

		data.castingTypes = {
			prepared: 'PF1.SpellPrepPrepared',
			spontaneous: 'PF1.SpellPrepSpontaneous',
			hybrid: 'Hybrid',
			prestige: 'Prestige'
		};

		data.config = pf1.config;
		return data;
	}
}
