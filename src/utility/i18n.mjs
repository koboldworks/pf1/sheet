export class i18n {
	/**
	 * @param {string} string i18n string path
	 * @returns {string}
	 */
	static localize = (string) => game.i18n.localize(string);

	/**
	 * @param {string} string i18n string path
	 * @param {object} mapping Mapping of keys to values.
	 * @returns {string}
	 */
	static format = (string, mapping) => game.i18n.format(string, mapping);

	/**
	 * Get localization
	 *
	 * @param {string} string
	 * @param {object} [mapping]
	 * @returns {string}
	 */
	static get = (string, mapping) => mapping ? this.format(string, mapping) : this.localize(string);

	/**
	 * Check if localization exists.
	 *
	 * @param {string} string
	 * @returns {boolean}
	 */
	static has = (string) => game.i18n.has(string);
}
