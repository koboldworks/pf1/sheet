export const prettyNumber = (num) => {
	const base = Math.floor(num),
		dec = Math.floor((num - base) * 100);

	const out = [];
	if (base > 0) out.push(`${base}`);
	else if (dec == 0) return '0';
	const dp = {
		50: '1/2', // ½
		33: '1/3', // ⅓
		66: '2/3', // ⅔
		25: '1/4', // ¼
		75: '3/4', // ¾
	}[dec];
	if (dp) out.push(dp);

	return out.join(' ');
}
