export const getVisibleName = (item) => {
	if (!game.user.isGM && item.showUnidentifiedData)
		return item.system.unidentified?.name || getIdentifiedName(item);
	return getIdentifiedName(item);
}

export const getIdentifiedName = (item) => {
	return item.system.identifiedName || item.name;
}

export const getUnidentifiedName = (item) => {
	return item.system.unidentified?.name || getIdentifiedName(item);
}

export const getWeightInfo = (item) => {
	const weight = item.system.weight;
	return {
		value: weight?.value ?? 0,
		total: weight?.converted.total ?? 0,
		unit: weight?.units,
		get have() { return Number.isFinite(this.value); },
		get zero() { return this.have && this.total === 0; },
	}
}

export const getValueInfo = (item) => {
	const system = item.system;
	const rv = {
		real: {
			value: system.price,
			get have() { return Number.isFinite(this.value); },
			get zero() { return this.have && this.value === 0; },
		},
		fake: {
			value: system.unidentified?.price,
			get have() { return Number.isFinite(this.value); },
			get zero() { return this.have && this.value === 0; },
		},
		get any() { return this.real.have || this.fake.have; },
		get anyZero() { return this.real.zero || this.fake.zero; },
		get zero() { return this.real.zero && this.fake.zero; },
	};
	rv.visible = item.showUnidentifiedData ? rv.fake : rv.real;
	return rv;
}
