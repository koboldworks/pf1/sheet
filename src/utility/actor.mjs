/**
 * Return total concentration bonus.
 *
 * @param actor ActorPF
 * @param book data.data.spells[book] structure
 * @returns
 */
export const calculateConcentration = (actor, book) => {
	const cl = book.cl.total;
	const ablMod = actor.system.abilities[book.ability]?.mod ?? 0;
	const bonus = book.concentration;

	const rollData = actor.getRollData();
	rollData.cl = cl;
	rollData.mod = ablMod;
	rollData.concentrationBonus = bonus;

	const roll = book.concentrationFormula.length ? RollPF.safeRoll(book.concentrationFormula, rollData).total : 0;

	return cl + ablMod + bonus.total + roll - rollData.attributes.energyDrain;
};

/**
 * Returns uniform object for all proficiencies.
 *
 * @param {object} p
 * @param {Array<string>} trl
 */
export const filterProficiency = (p, trl) => {
	let custom = 1;
	const rv = {};
	const customp = p.custom?.map(l => l?.trim()).filter(l => l?.length > 0) ?? [];
	for (const l of [...p.value, ...customp]) {
		const label = trl[l];
		if (label) rv[l] = label; // Built-in
		else rv[`_custom_${custom++}`] = l; // Custom
	}

	return rv;
};
