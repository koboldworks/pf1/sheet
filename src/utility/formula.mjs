import { safeEval } from './roll.mjs';

export const getFormulaVariables = (formula) => {
	/** @type {string[] | null} */
	const vars = formula.match(/@[\w.]+\b/g);
	/** @type {Set<string>} */
	const set = new Set();
	return vars
		?.map(v => v.slice(1).trim())
		.reduce((set, v) => set.add(v), set) ?? set;
}

export const resolveFormula = async (formula, rollData) => {
	const vars = getFormulaVariables(formula);
	// Formula requires actor
	/** @type {boolean} */
	const requireActor = vars?.some(v => !/^(?:item\.|cl$|sl$)/.test(v)) ?? false;
	if (requireActor) console.log('Unsupported vars:', vars);

	const roll = !requireActor ? safeEval(formula, rollData) : null;

	return {
		requireActor,
		variables: vars,
		roll,
		total: roll?.total
	}
}
