export const safeEval = (formula, rollData) => {
	let roll = {}
	try {
		roll = Roll.create(formula, rollData);
		roll.evaluate({ async: false });
	}
	catch (error) {
		Object.defineProperties(roll, {
			isInvalid: {
				get: () => true,
			},
			formula: {
				value: formula,
				enumerable: true,
			},
			error: {
				value: error,
				enumerable: true
			}
		});
		console.error('Bad formula:', formula);
	}
	return roll;
}
