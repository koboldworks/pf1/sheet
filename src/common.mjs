import { i18n } from './utility/i18n.mjs';

export function preventNull(event) {
	if (event.target.value?.length > 0) return; // OK
	event.target.value = event.target.defaultValue || i18n.get('Koboldworks.Error.Invalid');
}
