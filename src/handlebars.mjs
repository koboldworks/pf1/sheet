/* global RollPF */

/**
 * {{koboldworks-uses item number=false}}
 *
 * @param {ItemPF} item
 * @param {object} opts Hanlebars hash, ignore
 * @returns {string | number}
 */
Handlebars.registerHelper('koboldworks-uses', function (item, opts) {
	const numberOnly = opts.hash?.number ?? false;

	// const chargeCost = item.getDefaultChargeCost();
	const actionCost = item.defaultAction?.getChargeCost() ?? 0;

	if (actionCost != 0) {
		if (actionCost > 0) {
			const num = Math.floor(item.charges / item.chargeCost);
			return numberOnly ? num : `×${Math.floor(item.charges / item.chargeCost)}`;
		}
		else {
			return numberOnly ? -item.chargeCost : `+${-item.chargeCost}`;
		}
	}
	return numberOnly ? Infinity : '∞'; // ×Inf.
});

/**
 * {{koboldworks-iteratives item}}
 *
 * @param {ItemPF} item
 * @returns {Array<number>}
 */
Handlebars.registerHelper('koboldworks-iteratives', function (item) {
	const actor = item.actor,
		actorData = actor.system,
		action = item.defaultAction,
		actionData = action?.data ?? {},
		itemData = item.system,
		baseAtk = actorData.attributes.attack.shared + actorData.attributes.attack.general,
		rollData = item.getRollData(),
		ablMod = actor.system.abilities[actionData.ability?.attack]?.mod ?? 0;

	const bonus = RollPF.safeRollSync(actionData.attackBonus ?? '', rollData).total;
	const enh = Math.max(itemData.masterwork ? 1 : 0, itemData.enh ?? 0);

	// TODO: Account for conditional modifiers that are enabled by default.

	const attacks = [0];

	const extraAttacks = actionData.attackParts
		?.map(n => n[0])
		.filter(n => typeof n === 'number' || n?.trim()?.length > 0) ?? [];

	for (const a of extraAttacks) {
		const bonus = RollPF.safeRollSync(a, rollData);
		if (Number.isFinite(bonus.total))
			attacks.push(bonus.total);
	}

	const fmAtk = actionData.formulaicAttacks?.count?.formula?.trim();
	const fmAtkBonus = actionData.formulaicAttacks?.bonus?.formula?.trim() ?? '0';
	if (fmAtk?.length > 0) {
		const count = RollPF.safeRollSync(fmAtk, rollData);
		for (let i = 0; i < count.total; i++) {
			rollData.formulaicAttack = i;
			const bonus = RollPF.safeRollSync(fmAtkBonus, rollData);
			if (Number.isFinite(bonus.total))
				attacks.push(bonus.total);
		}
	}

	return attacks.map(a => a + baseAtk + ablMod + bonus + enh);

	/*
	data.meleeAttack = coreAttack + data.data.attributes.attack.melee + (meleeAtkAbl ?? 0);
		data.rangedAttack = coreAttack + data.data.attributes.attack.ranged + (rangedAtkAbl ?? 0);
		*/
});

/**
 * {{koboldworks-damage item}}
 *
 * @param {ItemPF} item
 * @returns {string}
 */
Handlebars.registerHelper('koboldworks-damage', function (item) {
	const actorData = item.actor.system,
		itemData = item.system,
		action = item.defaultAction,
		actionData = action?.data,
		rollData = item.getRollData();

	if (!action.hasDamage) return '0';

	const reducedParts = [];

	const handleParts = (parts = []) => {
		// eslint-disable-next-line prefer-const
		for (let { formula, type } of parts) {
			const roll = RollPF.safeRollSync(formula, rollData, undefined, undefined, { minimize: true });
			if (roll.total == 0) continue;

			formula = pf1.utils.formula.simplify(formula, rollData, { strict: false });
			formula = pf1.utils.formula.compress(formula);
			reducedParts.push(formula);
		}
	};

	const damage = actionData?.damage;
	if (!damage) return '0';

	try {
		handleParts(damage.parts);

		const dmgAbl = actionData.ability.damage;
		const dmgAblMod = Math.floor((actorData.abilities[dmgAbl]?.mod ?? 0) * (actionData.ability.damageMult || 1));
		if (dmgAblMod !== 0) reducedParts.push(dmgAblMod);

		handleParts(damage.nonCritParts);

		// TODO: Add conditional modifiers that are enabled by default.

		if (reducedParts.length === 0) reducedParts.push('0');
		const final = reducedParts.join('+').replace('+-', '-');
		return final;
	}
	catch (err) {
		console.error(err);
		return 'NaN';
	}
});

Handlebars.registerHelper('koboldworks-spell-range', function (item) {
	const ru = item.system.range.units;
	const tr = pf1.config.distanceUnits[ru];
	const rv = item.system.range.value;

	// const metric = game.settings.get('pf1', 'units') === 'metric';
	switch (ru) {
		case 'close':
		case 'medium':
		case 'long':
			return tr;
		case 'ft':
		case 'mi':
			if (rv) return `${rv} ${tr}`;
			break;
		case 'none':
			break;
		default:
			return tr;
	}
	return rv;
});

Handlebars.registerHelper('koboldworks-spell-components', function (item, opts) {
	const c = item.system.components;
	const m = item.system.materials;

	const compact = opts?.hash?.compact ?? false;

	const rv = [];

	if (c.verbal) rv.push('V');
	if (c.somatic) rv.push('S');

	if (c.value) {
		c.value.split(';')
			.map(cc => cc.trim()).filter(cc => cc.length)
			.forEach(cc => rv.push(cc));
	}
	if (c.material) {
		let mat = c.divineFocus === 2 ? 'M/DF' : 'M';
		if (!compact && m.value) mat = `${mat} (${m.value})`;
		rv.push(mat);
	}
	if (c.focus) {
		let focus = c.divineFocus === 3 ? 'F/DF' : 'F';
		if (!compact && m.focus) focus = `${focus} (${m.focus})`;
		rv.push(focus);
	}
	if (c.divineFocus === 1)
		rv.push('DF');

	return rv.join(' ');
});

Handlebars.registerHelper('koboldworks-activation', function (item, opts) {
	const unchained = game.settings.get('pf1', 'unchainedActionEconomy');
	const act = unchained ? item.system.unchainedAction?.activation : item.system.activation;
	const tr1 = unchained ? pf1.config.abilityActivationTypes_unchained : pf1.config.abilityActivationTypes;
	const trM = unchained ? pf1.config.abilityActivationTypesPlurals_unchained : pf1.config.abilityActivationTypesPlurals;

	if (act.cost <= 1) return tr1[act.type];
	else return `${act.cost} ${trM[act.type]}`;
});
