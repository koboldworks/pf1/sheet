export class SpellbookLevel {
	/**	@type {Number|null} */
	dcMode = null;
	spells = [];

	level;

	get enabled() {
		return this.level > 0 || this.#book.hasCantrips;
	}

	get left() {
		return this.#data.value ?? 0;
	}

	get max() {
		return this.#data.max ?? 0;
	}

	get label() {
		return `Koboldworks.Generic.Ordinals.${this.level}`;
	}

	#data;
	/** @type {Spellbook} */

	#book;

	constructor(level, data, book) {
		this.level = level;
		this.#data = data;
		this.#book = book;
	}
}
