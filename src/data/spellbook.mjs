// import { SpellbookLevel } from './spellbook-level.mjs';

/** @typedef {Actor} ActorPF */
/** @typedef {Item} ItemPF */
/** @typedef {ItemPF} ItemSpellPF */
/** @typedef {ItemPF} ItemClassPF */

export class Spellbook {
	/** @type {String} */
	id;
	/** @type {Boolean} */
	enabled;
	/** @type {String} */
	label;

	/** @type {ItemSpellPF[]} */
	allSpells = [];

	/** @type {Object.<Number, SpellbookLevel} */
	spells = {};

	get hasCantrips() {
		return this.#coreData.hasCantrips ?? false;
	}

	get isSpontaneous() {
		return this.#coreData.spellPreparationMode === 'spontaneous';
	}

	get isSpontaneousAlt() {
		return this.#coreData.spontaneous ?? true;
	}

	get isPrepared() {
		return this.#coreData.spellPreparationMode === 'prepared';
	}

	get isInnate() {
		return this.#coreData.class === '_hd';
	}

	get isCustom() {
		return this.#coreData.class === '';
	}

	/**	@returns {Number} */
	get concentration() {
		return this.#coreData.concentration.total;
	}

	/**	@returns {Number} */
	get cl() {
		return this.#coreData.cl.total;
	}

	#coreData;
	/** @type {Actor|ActorPF} */
	#actor;
	/** @type {Item|ItemClassPF} */
	#class;

	constructor(data, { id, actor, castingClass }) {
		this.id = id;
		this.#coreData = data;
		this.#actor = actor;
		this.#class = castingClass;
		this.label = data.label || this.#class?.name;
	}
}
