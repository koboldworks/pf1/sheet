/** @typedef {Actor & object} ActorPF */
/** @typedef {Item & object} ItemPF */
/** @typedef {ItemPF} ItemSpellPF */

export class SpellData {
	/** @type {ItemSpellPF} */
	#spell;
	#book;
	// #actor;

	get name() {
		return this.#spell.name;
	}

	get id() {
		return this.#spell.id;
	}

	get atWill() {
		return this.#spell.system.atWill ?? false;
	}

	get infiniteUses() {
		return !this.hasLimitedUses;
	}

	get isCharged() {
		return this.#spell.isCharged;
	}

	get charges() {
		return this.#spell.charges;
	}

	get maxCharges() {
		return this.#spell.maxCharges;
	}

	#chargeCost;
	get chargeCost() {
		// Cache
		this.#chargeCost ??= this.#spell.defaultAction?.getChargeCost() ?? 0;
		return this.#chargeCost;
	}

	get hasSave() {
		return this.#spell.hasSave;
	}

	get quickSlot() {
		return this.#spell.system.showInQuickbar ?? false;
	}

	get hasLimitedUses() {
		if (this.atWill) return false;
		const cost = this.chargeCost;
		return cost > 0;
	}

	getUsesLeft({ cost } = {}) {
		if (this.atWill) return Number.POSITIVE_INFINITY;
		cost ??= this.chargeCost;
		if (cost > 0) return Math.floor(this.charges / cost);
		else return Number.POSITIVE_INFINITY;
	}

	get useLabel() {
		const cost = this.chargeCost;
		if (cost == 0) return '∞';
		const left = this.getUsesLeft({ cost });
		if (left === Infinity) return '∞';
		if (cost > 0) {
			if (this.isPreparedCaster)
				return `${Math.floor(left / cost)}/${this.maxCharges}`;
			return `×${Math.floor(left / cost)}`;
		}

		return `+${-cost}`;
	}

	/** @typedef {number} */
	#dc;
	get DC() {
		if (this.hasSave && this.#dc === undefined) this.#dc = this.#spell.getDC();
		return this.#dc;
	}

	get isPreparedCaster() {
		return !this.#book.isSpontaneousAlt;
	}

	constructor(spell, level, book) {
		this.#spell = spell;
		// this.#actor = spell.actor;
		this.#book = book;
	}

	reset() {
		this.#chargeCost = undefined;
	}
}
