import { CFG } from './config.mjs';

import './handlebars.mjs'; // Register helpers

import './settings.mjs';

import { StatSheet } from './statsheet.mjs';
import { SpellbookSheet } from './spellbook.mjs';
import { DataSheet } from './datasheet.mjs';
import { InfoSheet } from './infosheet/_module.mjs';

import { ConfigureAbilities } from './dialogs/config-abilities.mjs';
import { ConfigureSpells } from './dialogs/config-spells.mjs';

Hooks.once('init', () => {
	// console.log('Koboldworks.Sheet | Initializing');
});

Hooks.once('ready', () => {
	console.log('Sheet-sss | Registering sheets');

	const npcTypes = ['npc'],
		pcTypes = ['character'],
		nonPCTypes = [...npcTypes, 'basic'],
		standardTypes = [...pcTypes, ...npcTypes],
		allTypes = [...nonPCTypes, ...pcTypes];

	Actors.registerSheet('Koboldworks', StatSheet, { label: 'Koboldworks.Sheet.Class.StatSheet', types: standardTypes, makeDefault: false });
	Actors.registerSheet('Koboldworks', SpellbookSheet, { label: 'Koboldworks.Sheet.Class.Spellbook', types: nonPCTypes, makeDefault: false });
	Actors.registerSheet('Koboldworks', DataSheet, { label: 'Koboldworks.Sheet.Class.DataSheet', types: nonPCTypes, makeDefault: false });

	Items.registerSheet('Koboldworks', InfoSheet, { label: 'Koboldworks.Sheet.Class.InfoSheet', types: InfoSheet.SUPPORTED_TYPES, makeDefault: false });

	const mod = game.modules.get(CFG.id);

	mod.api = {
		configure: {
			spellbooks: (actor, options) => new ConfigureSpells(actor).render(true, options),
			abilities: (actor, options) => new ConfigureAbilities(actor).render(true, options),
		}
	};

	console.log(`Sheet-sss | ${mod.version} | READY!`);
});
