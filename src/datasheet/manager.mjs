import { CFG } from '../config.mjs';
import { DataInstance } from './data-instance.mjs';

export class DatasheetManager {
	actor;
	data = {};

	sorted = [];

	constructor(actor) {
		this.actor = actor;
		this._transformData();
	}

	_transformData() {
		const allData = this.actor.getFlag(CFG.id, 'data') ?? {};

		const children = [];

		for (const e of Object.entries(allData)) {
			const [key, data] = e;
			const di = new DataInstance(data);
			di.id = key;
			allData[key] = di;

			if (di.parent) children.push(di);

			this.sorted.push(di);
		}

		for (const child of children) {
			const parent = this.data[child.parent];
			if (!parent) child.parent = null;
			else parent.addChild(child);
		}

		this.sort();

		this.data = allData;
	}

	has(id) {
		return id in this.data;
	}

	sort() {
		this.sorted.sort((a, b) => a.sort - b.sort);
	}

	/**
	 * Sort and save the new sorting data.
	 */
	async sortSave() {
		this.sort();
		let i = 1000;
		const update = {};
		for (const d of this.sorted) {
			d.sort = i;
			i += 1000;
			update[`${d.id}.sort`] = d.sort;
		}

		return this.actor.setFlag(CFG.id, 'data', update);
	}
}
