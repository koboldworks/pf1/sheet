export class DataInstance {
	name = null;
	type = 'number';
	value = null;
	priority = 0;
	sort = 1000;
	parent = null;
	children = [];
	#_id;
	options = {};

	constructor({ name, type, value, priority, sort, parent, options } = {}) {
		this.name = name;
		this.type = type ?? 'text';
		this.value = value;
		this.priority = priority ?? 0;
		this.sort = sort ?? 1000;
		this.parent = parent;
		this.options = options;

		this.transform();
	}

	static get types() {
		/**
		 * TODO: Add...
		 * - Actor
		 * - Item
		 * - Formula
		 */
		return {
			text: 'Text',
			number: 'Number',
			title: 'Title'
		};
	}

	transform() {
		switch (this.type) {
			case 'number': {
				if (typeof this.value !== 'number')
					this.value = parseFloat(this.value);
				break;
			}
			case 'title': {
				this.value = null;
				break;
			}
		}
	}

	set id(value) { this.#_id = value; }

	get id() { return this.#_id; }

	get isNumber() { return this.type === 'number'; }

	get isTitle() { return this.type === 'title'; }

	get isText() { return this.type === 'text'; }

	/**
	 * @param {DataInstance} child
	 */
	addChild(child) {
		child.parent?.removeChild(child);
		this.children.push(child);
		child.parent = this;
	}

	removeChild(child) {
		const i = this.children.findIndex(c => c.id === child.id);
		if (i < 0) return false;

		this.children.splice(i, 1);
		child.parent = null;
		return true;
	}

	toObject() {
		return {
			name: this.name,
			type: this.type,
			value: this.value,
			parent: this.parent?.id,
			priority: this.priority,
		};
	}
}
