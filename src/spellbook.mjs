import { CFG, templates } from './config.mjs';
import { preventNull } from './common.mjs';

const spellLevelLabel = {
	0: 'Cantrip/Orison/Knack',
	1: '1st level',
	2: '2nd level',
	3: '3rd level',
	4: '4th level',
	5: '5th level',
	6: '6th level',
	7: '7th level',
	8: '8th level',
	9: '9th level',
	10: 'Divine', // only accessble to deities
};

export class SpellbookSheet extends ActorSheet {
	hideEmpty = true;

	static get defaultOptions() {
		return mergeObject(super.defaultOptions, {
			classes: ['koboldworks', 'spellbook', 'pf1', 'sheet'],
			template: templates.spellbook,
			width: 600,
			height: 420,
			scrollY: ['form content .master-list'],
			dragDrop: [{ dragSelector: '[data-item-id]', dropSelector: '.drop-recipient' }],
			filters: [{ inputSelector: 'input[name="filter"]', contentSelector: '.master-list' }],
		});
	}

	async getData() {
		const data = await super.getData();

		data.uuid = `${this.actor.id}-spellbook-sheet`;

		const actorData = this.actor.system,
			rollData = this.actor.getRollData();

		// Mockup spellbook data for spells
		const books = rollData.spells;
		if (books === undefined) {
			/*
			const spellbooks = {};
			const book = { spontaneous: false };
			['primary', 'secondary', 'tertiary', 'spelllike'].forEach(b => spellbooks[b] = book);
			setProperty(actorData, bookPath, spellbooks);
			*/
		}

		// Prepare
		const spells = {};
		for (let i = 0; i < 10; i++)
			spells[i] = { level: i, items: [], label: spellLevelLabel[i] };

		let pages = 0;

		// Distribute per spell level
		data.actor.items
			.filter(i => i.type === 'spell')
			.forEach(s => {
				pages += Math.max(1, s.spellLevel);
				spells[s.spellLevel].items.push(s);
			});

		// Sort
		for (const sl of Object.keys(spells))
			spells[sl].items.sort((a, b) => a.data.sort - b.data.sort);

		// Finalize
		data.t = { spells, pages, schools: pf1.config.spellSchools };

		// console.log(data);

		data.hideEmpty = this.hideEmpty;

		return data;
	}

	async _deleteItem(ev) {
		const item = this._getItem(ev, { idOnly: true });
		if (item?.id) return this.actor.deleteEmbeddedDocuments('Item', [item.id]);
	}

	_getItem(ev, { idOnly = false } = {}) {
		const iel = ev.target.closest('[data-item-id]');
		const itemId = iel.dataset.itemId;
		return idOnly ? { id: itemId } : this.actor.items.get(itemId);
	}

	_renderItemDetails(ev) {
		const item = this._getItem(ev);
		// TODO
	}

	_editItem(ev) {
		const item = this._getItem(ev);
		item.sheet.render(true);
	}

	activateListeners(html) {
		super.activateListeners(html);

		html.find('.no-null').change(preventNull);

		html.find('action.delete').on('click', this._deleteItem.bind(this));

		/*
		html.find('[data-item-id]')
			.on('dragenter', function (ev) {
				this.classList.add('drag-hover');
			})
			.on('dragleave', function (ev) {
				// works poorly
				if (!ev.target.isSameNode(this))
					if (ev.target.closest('[data-item-id]')?.isSameNode(this))
						return;
				this.classList.remove('drag-hover');
			});
		*/

		html.find('input[name="filter"]').change(function (event) {
			// Prevent unnecessary sheet refresh
			if (event.target.value) {
				event.stopPropagation();
				event.preventDefault();
			}
		});

		html.find('[data-item-id]').on('click', this._renderItemDetails.bind(this));
		html.find('[data-item-id]').on('contextmenu', this._editItem.bind(this));

		html.find('input[name="hide-empty"]').on('click', (ev) => {
			ev.target.disabled = true; // re-enabled by re-render
			ev.preventDefault(); // blocks checkbox state change, but fixed by re-render
			ev.stopPropagation(); // prevent update
			this.hideEmpty = ev.target.checked; // save
			this.render();
		});
	}

	/**
	 * @override
	 */
	async _onDropItemCreate(itemData) {
		itemData = itemData instanceof Array ? itemData : [itemData];
		// Accept only spells
		itemData = itemData.filter(i => i.type === 'spell');
		return this.actor.createEmbeddedDocuments('Item', itemData);
	}

	/**
	 * Prevent active effect drops.
	 *
	 * @override
	 */
	async _onDropActiveEffect() {
		return false;
	}

	_onSearchFilter(event, query, rgx, html) {
		const isSearch = query.length > 0;
		const levels = html.querySelectorAll('.level-group');
		for (const doml of levels) {
			const spells = doml.querySelectorAll('.item[data-item-id] .item-name');
			let hidden = 0;
			for (const dom of spells) {
				const el = dom.closest('.item[data-item-id]');
				let match = true;
				if (isSearch) {
					match = rgx.test(dom.textContent);
					if (!match) hidden++;
				}
				el?.classList.toggle('hidden', !match);
			}

			if (isSearch || this.hideEmpty)
				doml.classList.toggle('hidden', hidden === spells.length);
			else
				doml.classList.remove('hidden');
		}
	}

	async _updateObject(event, data) {
		// console.log(data);
		this.hideEmpty = data['hide-empty'] ?? true;
		delete data['hide-empty']; // don't pass garbage to update object

		return super._updateObject(event, data);
	}
}

CFG.sheetClasses.spellbook.base = SpellbookSheet;
