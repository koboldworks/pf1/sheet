// Copied from Item Hints module.

export const stripFlairs = (formula) => formula.replace(/\[[^\]]*]/g, '');

export function simplifyFormula(formula, rollData) {
	const temp = [];
	const terms = RollPF.parse(stripFlairs(formula), rollData);
	for (const term of terms) {
		if (term instanceof DiceTerm || term instanceof OperatorTerm) {
			temp.push(term);
		}
		else if (term.isDeterministic) {
			const evl = RollPF.safeEval(term.formula, {}, { missing: 0, warn: true });
			temp.push(...RollPF.parse(`${evl}`));
		}
		else {
			temp.push(term);
		}
	}

	// Strip leading operators
	// while (temp[0] instanceof OperatorTerm) temp.shift(); // Ignored since this could be a - sign
	// Strip trailing operators
	// while (temp[temp.length - 1] instanceof OperatorTerm) temp.pop(); // Ignored because bad formula should be bad

	// Combine simple terms (e.g. 5+3)
	const temp2 = [];
	let prev, term;
	while (temp.length) {
		prev = term;
		term = temp.shift();
		const next = temp[0];
		if (term instanceof OperatorTerm) {
			// Ternary handling
			if (term.operator === '?') {
				temp.shift(); // remove if-true val
				const elseOp = temp.shift();
				const falseVal = temp.shift();
				const simpler = RollPF.safeEval([prev.formula, term.formula, next.formula, elseOp?.formula ?? '', falseVal?.formula ?? ''].join(''));
				term = RollPF.parse(`${simpler}`)[0];
				temp2.pop(); // Remove last term
			}
			else if (prev instanceof NumericTerm && next instanceof NumericTerm) {
				const simpler = RollPF.safeEval([prev.formula, term.formula, next.formula].join(''));
				term = RollPF.parse(`${simpler}`)[0];
				temp2.pop(); // Remove the last numeric term
				temp.shift(); // Remove the next term
			}
		}
		temp2.push(term);
	}

	return RollPF.simplifyTerms(temp2)
		.map(tt => tt.formula.trim()).join('');
}
