import { CFG } from './config.mjs';

const registerSheetsssSettings = () => {
	game.settings.register(CFG.id, CFG.SETTINGS.stripInfoSheet, {
		name: 'Koboldworks.Sheet.Settings.InfoSheet.Strip.Label',
		hint: 'Koboldworks.Sheet.Settings.InfoSheet.Strip.Hint',
		type: Boolean,
		default: true,
		scope: 'world',
		config: true
	});

	game.settings.register(CFG.id, CFG.SETTINGS.defaultInfoSheet, {
		name: 'Koboldworks.Sheet.Settings.InfoSheet.SoftDefault.Label',
		hint: 'Koboldworks.Sheet.Settings.InfoSheet.SoftDefault.Hint',
		type: Boolean,
		default: true,
		scope: 'client',
		config: true,
	});
}

Hooks.once('init', registerSheetsssSettings);
