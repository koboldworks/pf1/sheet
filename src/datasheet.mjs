import { CFG, templates } from './config.mjs';
import { DataInstance } from './datasheet/data-instance.mjs';
import { DatasheetManager } from './datasheet/manager.mjs';

// TODO: Custom actor class is desirable.

export class DataSheet extends ActorSheet {
	/** @type {DatasheetManager} */
	datamanager;

	openData = {};

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			classes: [...options.classes, 'koboldworks', 'actor', 'datasheet'],
			template: templates.datasheet,
			width: 600,
			height: 460,
			resizable: true,
			scrollY: ['content'],
			dragDrop: [{ dragSelector: '.data-list li[data-id] .grip', dropSelector: '.data-list .grip' }],
			// dragDrop: [{ dragSelector: '[data-item-id]', dropSelector: '.drop-recipient' }],
			// filters: [{ inputSelector: 'input[name="filter"]', contentSelector: '.master-list' }],
			submitOnChange: true,
			submitOnClose: true,
			closeOnSubmit: false,
		};
	}

	getData() {
		this.datamanager = new DatasheetManager(this.actor);

		for (const [id, editing] of Object.entries(this.openData)) {
			const data = this.datamanager.data[id];
			if (data) data.editing = editing;
			else delete this.openData[id];
		}

		const actor = this.actor;
		return {
			actor,
			prefix: `flags.${CFG.id}.data`,
			img: actor.img,
			name: actor.name,
			isOwner: actor.isOwner,
			data: this.datamanager,
			uuid: `datasheet-${this.actor.id}`,
			dataTypes: DataInstance.types,
			parentage: this.datamanager.sorted.reduce((rv, d) => {
				rv[d.id] = `${d.name ?? 'Unnamed'} [${d.id}]`;
				return rv;
			}, {}),
		};
	}

	/**
	 * @param {JQuery<HTMLElement>} html
	 */
	activateListeners(html) {
		super.activateListeners(html);

		if (!this.isEditable) return;

		html.find('action').on('click', this._onAction.bind(this));

		html.find('input.paint-on-select').on('click', this._onSelect.bind(this));
	}

	_transformNumberInput(input, original) {
		const re = input.match(/(?<exact>=)?(?<sign>[+-])?(?<value>\d+(\.\d+)?)/);
		if (!re) return input;
		const { exact, sign, value } = re.groups;
		let num = value ? parseFloat(value) : 0;
		if (!sign && !exact) return num;
		if (sign === '-') num = -num;
		if (exact == '=') return num;
		else return original + num;
	}

	async _onSelect(ev) {
		const el = ev.target;
		el.select();
	}

	_onAction(ev) {
		const el = ev.target.closest('[data-action]');
		const action = el.dataset.action;
		switch (action) {
			case 'create': {
				const odata = this.actor.getFlag(CFG.id, 'data') ?? {};
				const highest = Object.values(odata).reduce((a, d) => Math.max(d.sort ?? 0, a), 0);

				const data = { [randomID()]: { sort: highest + 1000, priority: 0 } };
				console.log('CreateData:', data);
				// console.log(this.element);
				return this.actor.setFlag(CFG.id, 'data', data);
			}
			case 'delete': {
				const lel = el.closest('li[data-id]');
				const id = lel.dataset.id;
				console.log('Delete:', id);
				const data = this.datamanager.data[id];
				Dialog.confirm({
					title: 'Delete data?',
					content: `<h4>${data.name}</h4><p><label>ID: ${data.id}</label><p><label>Value:</label><p>${data.value}`,
					yes: () => this.actor.setFlag(CFG.id, 'data', { [`-=${id}`]: null }),
					defaultYes: true,
				});
				break;
			}
			case 'edit': {
				const lel = el.closest('li[data-id]');
				lel.classList.toggle('editing');
				this.openData[lel.dataset.id] = lel.classList.contains('editing');
				break;
			}
		}
	}

	_onDragStart(event) {
		const el = event.target.closest('[data-id]');
		const id = el.dataset.id;

		const actor = this.actor;

		const dragData = {
			actorId: actor.id,
			sceneId: actor.isToken ? canvas.scene?.id : null,
			tokenId: actor.isToken ? actor.token.id : null,
			id,
			type: 'Datasheet.Data',
		};

		// Set data transfer
		event.dataTransfer.setData('text/plain', JSON.stringify(dragData));
		event.dataTransfer.setDragImage(el.querySelector('.name'), 20, 20);
	}

	_onDrop(event) {
		event.preventDefault();
		event.stopPropagation();

		let data;
		try {
			data = JSON.parse(event.dataTransfer.getData('text/plain'));
		}
		catch (err) {
			console.error(err);
			return false;
		}

		if (data.type !== 'Datasheet.Data')
			return super._onDrop(event);

		if (data.actorId !== this.actor.id) {
			return ui.notifications?.warn('Datasheet does not currently support copying data between sheets');
		}

		const sourceId = data.id;

		const el = event.target.closest('[data-id]');
		const targetId = el.dataset.id;

		if (sourceId === undefined) {
			console.warn('No source');
			return;
		}
		if (targetId === undefined) {
			console.warn('No target');
			return;
		}
		if (sourceId === targetId) {
			console.warn('Source is same as target');
			return;
		}

		const i = this.datamanager.sorted.findIndex(i => i.id === targetId);
		if (i < 0) {
			console.log('Target not found');
			return;
		}
		const prev = this.datamanager.sorted[i - 1],
			target = this.datamanager.sorted[i];

		if (prev?.id === sourceId) {
			console.warn('Sorting onto old location');
			return;
		}

		let sort = 0;
		if (prev) sort = prev.sort + Math.floor((target.sort - prev.sort) / 2);
		else sort = target.sort - 100;
		// TODO: Bubble

		this.datamanager.data[sourceId].sort = sort;

		this.datamanager.sortSave();// .then(_ => this.render());
	}

	/**
	 * @param {Event} event
	 * @param {object} formData
	 */
	_updateObject(event, formData) {
		// Adjust relative values
		const el = event.target;
		if (el?.name && el.classList.contains('relative')) {
			const id = el.closest('.entry[data-id]')?.dataset.id;
			if (id) {
				const value = el.value;
				if (value) {
					const nv = this._transformNumberInput(value, this.datamanager.data[id].value);
					formData[el.name] = nv;
				}
				else {
					formData[el.name] = 0;
				}
			}
		}

		formData = expandObject(formData);

		/*
		// Transformations
		for (const [id, entry] of Object.entries(formData.flags[CFG.id].data)) {
			for (const [key, value] of Object.entries(entry)) {
				// Boo
			}
		}
		*/

		return super._updateObject(event, formData);
	}
}

CFG.sheetClasses.datasheet.base = DataSheet;
