import { CFG, templates, flatTemplates } from './config.mjs';
import { filterProficiency } from './utility/actor.mjs';
import { preventNull } from './common.mjs';

import { ConfigureAbilities } from './dialogs/config-abilities.mjs';
import { ConfigureSpells } from './dialogs/config-spells.mjs';

import { SpellbookLevel } from './data/spellbook-level.mjs';
import { Spellbook } from './data/spellbook.mjs';
import { SpellData } from './data/spelldata.mjs';

import { i18n } from './utility/i18n.mjs';

/**
 * @param {PointerEvent} ev
 */
const getSkipActionPrompt = (ev) => {
	const shift = ev.shiftKey;
	const skip = game.settings.get('pf1', 'skipActionDialogs');
	return skip && !shift || !skip && shift;
};

const soakEvent = (ev) => {
	ev.stopPropagation();
	ev.preventDefault();
	return ev;
};

export class StatSheet extends ActorSheet {
	toggled = {
		skills: false,
		feats: false,
	};

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			classes: [...options.classes, 'koboldworks', 'statsheet', 'pf1', 'sheet'],
			template: flatTemplates.statsheet,
			width: 600,
			height: 620,
			scrollY: ['form'],
			tabs: [
				/*
				{
					navSelector: '.tab-selector',
					contentSelector: '.tab-content',
					initial: 'summary',
				}
				*/
			],
			dragDrop: [
				{
					dragSelector: 'action[data-item-id]',
					dropSelector: '.drop-recipient',
				},
			],
		};
	}

	/**
	 * Feed info to the sheet.
	 */
	async getData() {
		const actor = this.actor,
			system = actor.system,
			rollData = actor.getRollData();

		const context = {
			actor,
			system,
			config: pf1.config,
			isNPC: actor.type === 'npc' && !actor.hasPlayerOwner,
			owner: actor.isOwner,
			editable: actor.isOwner,
			itemTypes: actor.itemTypes,
			classes: {
				normal: [],
				prestige: [],
				racial: [],
				mythic: [],
			},
		};

		// Simplify class types
		const classMap = { base: 'normal', npc: 'normal' };
		context.itemTypes.class.forEach(cls => {
			context.classes[classMap[cls.subType] ?? cls.subType].push(cls);
		});

		Object.values(context.classes).forEach(cls => cls.sort((a, b) => b.system.level - a.system.level));

		context.allClasses = [...context.classes.normal, ...context.classes.prestige, ...context.classes.racial, ...context.classes.mythic];

		/** Mobility */

		const speeds = {};
		for (const [key, value] of Object.entries(system.attributes.speed)) {
			if (value.total > 0) speeds[key] = {
				value: value.total,
				label: `PF1.Speed${key.capitalize()}_Short`,
				maneuverability: value.maneuverability,
			};
		}

		/** Skills */

		let hiddenSkills = 0;
		const skills = {
			count: 0,
			items: {},
		};
		const simplifySkill = (key, label, data) => {
			skills.count++;
			if (!label) label = pf1.config.skills[key];
			if (!(data.rank > 0)) hiddenSkills++;
			if (data.rt && data.rank == null) return;
			skills.items[key] = { key, label, rank: data.rank, mod: data.mod };
		};

		for (const [key, value] of Object.entries(system.skills)) {
			if (value.subSkills) {
				for (const [key2, value2] of Object.entries(value.subSkills))
					simplifySkill(`${key}.subSkills.${key2}`, value2.name, value2);
			}
			else
				simplifySkill(key, null, value);
		}

		/** Languages */
		const languages = {
			items: filterProficiency(system.traits.languages, pf1.config.languages),
			get count() { return Object.keys(this.items).length; },
		};

		/** Attacks */

		const attacks = {
			melee: [],
			ranged: [],
			maneuver: [],
		};

		const allAttacks = context.itemTypes.attack;
		attacks.ranged = allAttacks.filter(i => ['rwak', 'rsak'].includes(i.defaultAction?.data.actionType));
		attacks.melee = allAttacks.filter(i => ['mwak', 'msak'].includes(i.defaultAction?.data.actionType));
		attacks.maneuver = allAttacks.filter(i => ['rcman', 'mcman'].includes(i.defaultAction?.data.actionType));

		/** Spellcating */

		const spells = {
			caster: false,
			book: {},
			attack: [],
			utility: [],
		};

		const allSpells = context.itemTypes.spell;
		const allReadySpells = allSpells.filter(i => i.isCharged);

		const activeSpellbooks = [];

		const books = rollData.spells;

		allSpells.forEach(s => {
			const bookId = s.system.spellbook;
			const bookData = books[bookId];
			if (spells.book[bookId] === undefined) {
				if (bookData.inUse) activeSpellbooks.push(bookId);
				const classId = bookData.class,
					castingClass = context.itemTypes.class.find(i => i.system.tag === classId);

				spells.book[bookId] = new Spellbook(bookData, { id: bookId, actor, castingClass });
			}
			const book = spells.book[bookId];
			// console.log(bookId, book, spells.book);

			const level = s.system.level;
			if (book.spells[level] === undefined) {
				const levelData = bookData.spells[`spell${level}`];
				book.spells[level] = new SpellbookLevel(level, levelData, book);
			}

			const spell = new SpellData(s, book.spells[level], book);
			book.allSpells.push(spell);
			book.spells[level].spells.push(spell);
		});

		/**
		 * Calculate mode for numbers.
		 *
		 * @param nums
		 */
		const mode = (nums) => {
			const numCounts = {};
			nums.filter(n => n !== undefined) // Remove invalid data
				.forEach(n => numCounts[n] = (numCounts[n] ?? 0) + 1); // Add up

			// Find most commonly occurring number
			let mnum = 0, mnumCount = 0;
			for (const [n, count] of Object.entries(numCounts)) {
				if (count > mnumCount) {
					mnum = n;
					mnumCount = count;
				}
			}

			return parseInt(mnum, 10);
		};

		Object.keys(spells.book).forEach(b => {
			const book = spells.book[b];
			for (const [level, data] of Object.entries(book.spells)) {
				if (data.spells.length === 0) {
					// console.log('---- NO SPELLS FOR LEVEL:', level);
					delete book.spells[level];
					continue;
				}

				const dcs = data.spells.map(s => s.DC);
				data.dcMode = dcs.length ? mode(dcs) : null; // Get most commonly occurring DC
			}
		});

		spells.attack = allReadySpells.filter(i => i.hasDamage && !i.isHealing);
		spells.utility = allReadySpells.filter(i => !spells.attack.find(i2 => i2.id === i.id));
		spells.caster = activeSpellbooks.length > 0;
		spells.spellbooks = activeSpellbooks;

		// console.log('Spells:', allSpells);
		// console.log(spells);

		/** Abilities */

		const abilities = {
			attack: [],
			other: [],
			passive: [],
		};

		let hiddenPassiveAbilities = 0, hiddenOtherAbilities = 0, hiddenAttackAbilities = 0;
		const allAbilities = context.itemTypes.feat;
		abilities.attack = allAbilities.filter(i => (i.hasDamage || i.hasSave) && i.hasAction);
		abilities.attack.forEach(i => {
			if (!i.isActive) {
				hiddenAttackAbilities++;
				i.__hidden = true;
			}
		});
		abilities.other = allAbilities.filter(i => !(i.hasDamage || i.hasSave) && i.hasAction);
		abilities.other.forEach(i => {
			if (!i.isActive) {
				hiddenOtherAbilities++;
				i.__hidden = true;
			}
		});
		abilities.passive = allAbilities.filter(i => !i.hasAction);
		abilities.passive.forEach(i => {
			if (!i.isActive || i.system.changes.length === 0 && i.system.contextNotes.length === 0) {
				hiddenPassiveAbilities++;
				i.__hidden = true;
			}
		});

		// console.log('Abilities:', allAbilities);
		// console.log(abilities);

		/** Resistances and Vulnerabilities */

		const filterTrait = (p) => {
			if (typeof p === 'string') p = p.split(';');
			return p.map(t => t?.trim()).filter(t => t?.length);
		};

		const resistances = [
			...filterTrait(system.traits.dr.custom),
			...filterTrait(system.traits.eres.custom),
		].filter(i => i?.length > 0);

		const vulnerabilities = [...system.traits.dv.value, ...filterTrait(system.traits.dv.custom)].filter(i => i?.length > 0);

		const immunities = [
			...filterTrait(system.traits.cres),
			...system.traits.di.value,
			...filterTrait(system.traits.di.custom),
			...system.traits.ci.value,
			...filterTrait(system.traits.ci.custom),
		].filter(i => i?.length > 0);

		/** Usable Equipment */

		const itemTypes = context.itemTypes;

		const itemSort = (a, b) => a.sort - b.sort;

		Object.values(itemTypes).forEach(l => l.sort(itemSort));

		const activatableEquipment = [
			...itemTypes.weapon.filter(i => i.hasAction),
			...itemTypes.equipment.filter(i => i.hasAction),
			...itemTypes.consumable,
		];

		// Usable equipment
		const equipment = {
			active: activatableEquipment,
			misc: actor.items.filter(i => !i.hasAction && ['equipment', 'weapon', 'container'].includes(i.type)),
			loot: context.itemTypes.loot,
		};

		const hiddenActiveEq = equipment.active.reduce((t, i) => {
				if (i.system.quantity == 0 || (!i.system.equipped && i.type !== 'consumable')) {
					t++;
					i.__hidden = true;
				}
				return t;
			}, 0),
			hiddenMiscEq = equipment.misc.reduce((t, i) => {
				if (i.system.quantity == 0 || !i.system.equipped) {
					t++;
					i.__hidden = true;
				}
				return t;
			}, 0),
			hiddenLootEq = equipment.loot.length;

		/** Buffs */
		const buffs = context.itemTypes.buff;

		const hidden = {
			skills: hiddenSkills,
			abilities: {
				passive: hiddenPassiveAbilities,
				other: hiddenOtherAbilities,
				attack: hiddenAttackAbilities,
			},
			equipment: {
				// (and (eq data.data.quantity 0) (and (eq data.data.equipped false) (ne type 'consumable')))
				active: hiddenActiveEq,
				misc: hiddenMiscEq,
				loot: hiddenLootEq,
			},
			languages: false,
			buffs: false,
		};

		const labels = {
			saves: {
				fort: i18n.get('Koboldworks.Missing.PF1.SaveFortShort'),
				ref: i18n.get('Koboldworks.Missing.PF1.SaveRefShort'),
				will: i18n.get('Koboldworks.Missing.PF1.SaveWillShort'),
			},
		};

		/** Collate all data */

		const senses = duplicate(system.traits.senses);
		senses.custom = senses.custom.split(';').map(s => s.trim()).filter(s => s != null);
		senses.enabled = senses.dv > 0 || senses.ll.enabled || senses.bs > 0 || senses.ts > 0 || senses.sid == true || senses.ts == true || senses.si == true;

		context.t = {
			traits: system.traits,
			subTypes: actor.race?.system.subTypes?.length > 0,
			speeds,
			effectiveHP: system.attributes.hp.value + system.attributes.hp.temp,
			resistances,
			vulnerabilities,
			immunities,
			senses,
			skills,
			attacks,
			abilities,
			buffs,
			languages,
			spells,
			equipment,
			hidden,
			labels,
		};

		context.enriched = {
			notes: await TextEditor.enrichHTML(actor.system.details.notes.value, { rollData, secrets: actor.isOwner, async: true }),
		};

		// console.log('WIP | StatSheet | getData.data ', data);
		// console.log('WIP | StatSheet | getData.this ', this);

		return context;
	}

	async _itemControlDialog(item) {
		if (!item) return;

		const actor = this.actor;

		return new Promise((resolve) => {
			new Dialog({
				title: i18n.get('PF1.DeleteItemTitle', { 0: item.name }),
				content: i18n.get('PF1.DeleteItemConfirmation') + '<hr>',
				buttons: {
					delete: {
						label: i18n.get('Delete'),
						callback: async () => resolve(await actor.deleteEmbeddedDocuments('Item', [item.id])),
					},
					cancel: {
						label: i18n.get('Cancel'),
						callback: () => resolve(),
					},
				},
				default: 'cancel',
			}).render(true);
		});
	}

	_createBaseItem(event, key) {
		event.preventDefault();
		event.stopPropagation();
		console.log('Creating item:', key);
		console.log('Templates:', game.system.template.Item);
		const template = game.system.template.Item[key];
		if (!template) {
			console.error(`Could not find template for '${key}' to create item from.`);
			return;
		}
		let data = duplicate(template);
		for (const t of data.templates) mergeObject(data, duplicate(game.system.template.Item.templates[t]));
		delete data.templates;
		data = {
			name: '<< Unnamed >>',
			img: 'icons/svg/sun.svg',
			type: key,
			data,
		};
		switch (key) {
			case 'feat':
				data.name = i18n.get('Koboldworks.Sheet.Abilities.NewName');
				break;
			case 'attack':
				data.name = i18n.get('Koboldworks.Sheet.Attacks.NewName');
				break;
			case 'buff':
				data.name = i18n.get('Koboldworks.Sheet.Buffs.NewName');
				break;
			case 'equipment':
				data.name = i18n.get('Koboldworks.Sheet.Buffs.NewName');
				break;
		}
		console.log('Creating item:', data);
		this.actor.createEmbeddedDocuments('Item', [data]);
	}

	/**
	 * @param {JQuery} jq
	 * @override
	 */
	activateListeners(jq) {
		super.activateListeners(jq);
		const html = jq[0];

		html.querySelectorAll('.no-null')
			?.forEach(el => el.addEventListener('change', preventNull));

		const actor = this.actor,
			sheet = this;

		// Nav buttons
		html.querySelectorAll('nav ol li a[data-scroll-to]')
			?.forEach(el => el.addEventListener('click', ev => this._scrollTo(ev, el)));

		function openItemSheet(event, itemId) {
			const item = actor.items.get(itemId);
			if (event.altKey) sheet._itemControlDialog(item);
			else item.sheet.render(true);
		}

		// Register actions
		function actionRegistrar(el) {
			const key = el.dataset.key;
			let unrecognized = false;
			switch (el.dataset.category) {
				case 'skill':
					el.addEventListener('click', (ev) => actor?.rollSkill(key, { skipDialog: getSkipActionPrompt(ev) }));
					el.addEventListener('contextmenu', () => sheet._editSkill(el));
					break;
				case 'ability':
					el.addEventListener('click', (ev) => actor?.rollAbility(key, { skipDialog: getSkipActionPrompt(ev) }));
					break;
				case 'save':
					el.addEventListener('click', (ev) => actor?.rollSavingThrow(key, { skipDialog: getSkipActionPrompt(ev) }));
					break;
				case 'init':
					el.addEventListener('click', () => actor?.rollInitiative({ createCombatants: true, rerollInitiative: game.user.isGM }));
					break;
				case 'defenses':
					el.addEventListener('click', () => actor?.rollDefenses());
					break;
				case 'buff':
					el.addEventListener('click', () => sheet._toggleBuff(el));
					el.addEventListener('contextmenu', (ev) => openItemSheet(ev, el.dataset.itemId));
					break;
				case 'special-abilities':
				case 'equipment':
					el.addEventListener('click', (ev) => actor.items.get(el.dataset.itemId)?.use({ skipDialog: getSkipActionPrompt(ev) }));
					el.addEventListener('contextmenu', (ev) => openItemSheet(ev, el.dataset.itemId));
					break;
				case 'other':
					if (key === 'item')
						el.addEventListener('contextmenu', (ev) => openItemSheet(ev, el.dataset.itemId));
					break;
				case 'action': {
					if (key === 'rest') {
						el.addEventListener('click', () => {
							const dialog = pf1.applications.actor.ActorRestDialog;
							const app = Object.values(actor.apps).find((o) => o instanceof dialog && o.element);
							if (app) app.bringToTop();
							else new dialog(actor).render(true);
						});
					}
					break;
				}
				case 'toggle':
					el.addEventListener('click', sheet._toggle.bind(sheet));
					break;
				case 'caster':
					switch (key) {
						case 'caster-level':
							el.addEventListener('click', () => actor?.rollCL(el.dataset.book));
							break;
						case 'concentration':
							el.addEventListener('click', () => actor?.rollConcentration(el.dataset.book));
							break;
						default:
							unrecognized = true;
							break;
					}
					break;
				case 'spell':
				case 'attack':
					switch (key) {
						case 'cmb':
							el.addEventListener('click', () => actor?.rollCMB());
							break;
						case 'bab':
							el.addEventListener('click', () => actor?.rollBAB());
							break;
						case 'item':
							el.addEventListener('click', (ev) => actor.items.get(el.dataset.itemId)?.use({ skipDialog: getSkipActionPrompt(ev) }));
							el.addEventListener('contextmenu', (ev) => openItemSheet(ev, el.dataset.itemId));
							break;
						default:
							unrecognized = true;
							break;
					}
					break;
				case 'add':
					// $(this).on('click', (ev) => sheet._createBaseItem(ev, key));
					break;
				case 'configure':
					el.addEventListener('click', () => sheet._openConfig(el.dataset.config));
					break;
				default:
					unrecognized = true;
					break;
			}

			if (unrecognized) {
				console.warn('STATSHEET | UNRECOGNIZED ACTION REGISTRATION:', el);
			}
		}

		html.querySelectorAll('action')
			.forEach(actionRegistrar);

		jq.find('.sizer').on('click', sheet._toggle.bind(sheet));

		// Restore toggle state.
		// console.log('Attempting to preserve toggle state');
		html.querySelectorAll('[data-category="toggle"]')?.forEach(function (el) {
			if (sheet.toggled[el.dataset.for])
				el.click();
		});
	}

	_openConfig(config) {
		switch (config) {
			case 'abilities':
				return new ConfigureAbilities(this.actor).render(true, { focus: true });
			case 'spells':
				return new ConfigureSpells(this.actor).render(true, { focus: true });
			default:
				ui.notifications?.warn(`Attempting to open unrecongized config manager: ${config}`);
		}
	}

	_scrollToOffset(offset = 0, { correction = true, smooth = false } = {}) {
		const form = this.element.find('form').get(0);
		if (correction) offset -= form.offsetTop;
		form.scrollTo({ top: Math.max(0, offset), behavior: smooth ? 'smooth' : 'auto' });
	}

	_scrollTo(event, el) {
		event.preventDefault();
		event.stopPropagation();
		const target = el.dataset?.scrollTo;
		if (!target) return;
		const scrollTo = this.element[0]?.querySelector(`[data-scroll-target=${target}`);
		if (scrollTo) this._scrollToOffset(scrollTo.offsetTop, { smooth: true });
	}

	_toggle(ev) {
		const el = ev.target;
		const ed = el.dataset;
		if (ed.category !== 'toggle' || ed.key !== 'visibility') {
			console.log('Unexpected click target data:', ed);
			return;
		}

		const section = el.closest(`section[name="${ed.for}"]`);
		if (!section) return;
		const collapser = 'collapser', expander = 'expander', hidden = 'hidden', unhidden = 'unhidden';
		const collapsing = el.classList.contains(expander);

		// Save toggle state in temporary manner
		this.toggled[ed.for] = collapsing;

		$(section).find('ol li').each(function () {
			if (collapsing && this.classList.contains(hidden)) {
				this.classList.remove(hidden);
				this.classList.add(unhidden);
			}
			else if (!collapsing && this.classList.contains(unhidden)) {
				this.classList.add(hidden);
				this.classList.remove(unhidden);
			}
		});

		if (collapsing) {
			el.classList.add(collapser);
			el.classList.remove(expander);
		}
		else {
			el.classList.add(expander);
			el.classList.remove(collapser);
		}
	}

	/** @override */
	async _render(force, options) {
		await super._render(force, options);

		// Hack to make this._scrollPositions to actually work
		const offset = this._scrollPositions?.form ?? 0;
		if (offset > 0) this._scrollToOffset(offset, { correction: false });
	}

	async _toggleBuff(element) {
		const id = element.dataset.itemId;
		const buff = this.actor.items.get(id);

		if (buff) {
			console.log('Toggling buff', buff.name, 'to', !buff.system.active);
			buff.update({ 'data.active': !buff.system.active });
		}
		else {
			console.error('BUFF NOT FOUND:', id);
		}
	}

	async _editSkill(element) {
		const id = element.dataset.key;
		const dialogId = `actor-${this.actor.id}-skill-${id}`;

		// Check for already open dialog
		const oldDialog = Object.values(ui.windows).find(w => w.id === dialogId);
		if (oldDialog?.rendered)
			return void oldDialog?.render(true, { focus: true });

		const info = this.actor.getSkillInfo(id);
		if (!info)
			return void console.warn('Couldn\'t retrieve skill info for:', id);

		// Fill in what's missing from getSkillInfo
		const d = this.actor.system.skills[id];
		// console.log(info);
		info.ability = d.ability;
		info.cs = d.cs;
		info.acp = d.acp;
		info.actorId = this.actor.id;
		info.skillId = id;
		info.subName = d.name?.trim();
		info.subSkill = info.parentSkill;

		info.config = { abilities: pf1.config.abilities };

		const content = await renderTemplate(templates.dialog.skill, info);
		const dom = await new Promise(resolve => new Dialog(
			{
				title: info.name,
				content,
				buttons: {
					save: {
						label: i18n.get('Save'),
						callback: html => resolve(html.querySelector('form')),
					},
				},
				close: _ => resolve(null),
				default: 'save',
			},
			{
				id: dialogId,
				width: 280,
				jQuery: false,
			},
		).render(true));

		if (!dom) return;

		const formData = new FormDataExtended(dom).object;

		const nd = {
			ability: formData.ability,
			rank: formData.rank,
			rt: formData.rt,
			acp: formData.acp,
			name: formData.name?.trim(),
		};

		const updateData = {};
		for (const k of Object.keys(nd))
			if (info[k] !== nd[k] && nd[k] !== undefined)
				updateData[`system.skills.${id}.${k}`] = nd[k];

		if (Object.keys(updateData).length === 0) return;

		return this.actor.update(updateData);
	}
}

CFG.sheetClasses.statsheet.base = StatSheet;
